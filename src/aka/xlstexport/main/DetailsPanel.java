package aka.xlstexport.main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.text.NumberFormat;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.eclipse.jdt.annotation.NonNull;

import aka.xlstexport.main.config.PropertyPreference;
import aka.xlstexport.main.constants.ExportType;
import aka.xlstexport.main.constants.PropertyConstants;
import aka.xlstexport.main.filters.FilesFilter;
import aka.xlstexport.main.helpers.AbstractExportHelper;
import aka.xlstexport.main.helpers.context.FilesContext;
import aka.xlstexport.main.helpers.context.HTMLFileContext;
import aka.xlstexport.main.helpers.context.ImageContext;
import aka.xlstexport.main.helpers.context.MediaContext;
import aka.xlstexport.main.helpers.game.GameExportHelper;
import aka.xlstexport.main.helpers.music.MusicExportHelper;
import aka.xlstexport.main.listeners.CheckBoxListener;
import aka.xlstexport.main.listeners.ExportEventListener;
import aka.xlstexport.main.listeners.TextListener;
import aka.xlstexport.main.runnable.ExportRunnable;

/**
 * Details panel.
 *
 * @author Akazukin
 */
public class DetailsPanel extends JPanel implements ActionListener, ItemListener {

	private static final long serialVersionUID = -842801877273513751L;
	private JButton xmlFileButton;
	private JFileChooser fileChooser;
	private JTextField jXMLTextField;
	private File xmlFile;
	private JTextField jXSLIndexTextField;
	private JButton xslIndexFileButton;
	private File xslIndexFile;
	private JTextField jXSLDetailsTextField;
	private JButton xslDetailsFileButton;
	private File xslDetailsFile;
	private JCheckBox jExportMP3CheckBox;
	private JCheckBox jExportImage;
	private JCheckBox jCreateThumbnailImage;
	private JFormattedTextField thumbnailWidth;
	private JPanel thumbnailPanel;
	private JTextField jExportDirTextField;
	private JButton exportDirButton;
	private File exportDir;
	@NonNull
	private final FilesFilter xmlFilter = new FilesFilter(FilesFilter.XML);
	@NonNull
	private final FilesFilter xslFilter = new FilesFilter(FilesFilter.XSL);
	@NonNull
	private final FilesFilter cssFilter = new FilesFilter(FilesFilter.CSS);
	@NonNull
	private final Preferences preferences;
	private JButton exportButton;
	private ExportEventListener exportEventListener;
	private ExportType exportType;
	private JFormattedTextField splitIndexEvery;
	private JTextField jBrutCopyTextField;
	private File brutCopyDir;
	private JButton brutCopyDirButton;
	private JTextField jCSSTextField;
	private File cssFile;
	private JButton cssFileButton;
	private JCheckBox jsortByTitle;
	private JCheckBox compressCSS;
	private JCheckBox obfuscateJS;
	private JCheckBox copyNewDifferentSize;
	private JCheckBox compressHTML;
	private JCheckBox splitByPlatform;
	private JTextField jXSLPlatformTextField;
	private File xslPlatformFile;
	private JButton xslPlatformFileButton;

	/**
	 * Constructor.
	 *
	 * @param preferencesParam
	 *            Storing preferences management.
	 */
	public DetailsPanel(@NonNull final Preferences preferencesParam) {
		this.preferences = preferencesParam;
		setLayout(new GridBagLayout());
		setBackground(Color.WHITE);
		this.exportType = ExportType.GAME;
		processChangeExportType();

		this.fileChooser = new JFileChooser();
		this.fileChooser.setAcceptAllFileFilterUsed(false);
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final Object component = e.getSource();
		if (component == this.xmlFileButton) {
			processXMLFileButton();
		} else if (component == this.xslIndexFileButton) {
			processXSLIndexFileButton();
		} else if (component == this.xslDetailsFileButton) {
			processXSLDetailsFileButton();
		} else if (component == this.cssFileButton) {
			processCSSFileButton();
		} else if (component == this.exportDirButton) {
			processExportDirButton();
		} else if (component == this.brutCopyDirButton) {
			processBrutCopyDirButton();
		} else if (component == this.exportButton) {
			processExportButton();
		} else if (component == this.xslPlatformFileButton) {
			processXSLPlatformFileButton();
		}
	}

	private void processExportButton() {
		final HTMLFileContext htmlIndexContext = getHtmlIndexContext();
		final ImageContext imageContext = getImageContext();
		final ExportEventListener currentListener = this.exportEventListener;

		if (currentListener != null) {
			final FilesContext fileContext = new FilesContext();
			fileContext.setCommonDir(this.brutCopyDir);
			fileContext.setCopyNewDifferentSize(this.copyNewDifferentSize.isSelected());
			fileContext.setCssFile(this.cssFile);
			fileContext.setCompressCSS(this.compressCSS.isSelected());
			fileContext.setObfuscateJS(this.obfuscateJS.isSelected());
			// process Export
			AbstractExportHelper exportHelper = null;
			if (ExportType.MUSIC == this.exportType) {
				final MediaContext mediaContext = new MediaContext(this.jExportMP3CheckBox.isSelected());
				exportHelper = new MusicExportHelper(currentListener, htmlIndexContext, imageContext, fileContext, mediaContext);
			} else if (ExportType.GAME == this.exportType) {
				exportHelper = new GameExportHelper(currentListener, htmlIndexContext, imageContext, fileContext);
			} else if (ExportType.BOOK == this.exportType) {
				//
			}
			if (exportHelper != null) {
				final JButton button = this.exportButton;
				assert button != null;
				final Runnable runnable = new ExportRunnable(exportHelper, button, currentListener);
				final Thread thread = new Thread(runnable);
				thread.start();
			}
		}
	}

	private void processBrutCopyDirButton() {
		final String dir = this.jBrutCopyTextField.getText();
		if (dir != null) {
			this.fileChooser = new JFileChooser(new File(dir));
			this.fileChooser.setAcceptAllFileFilterUsed(false);
		}
		this.fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		this.fileChooser.removeChoosableFileFilter(this.xslFilter);
		this.fileChooser.removeChoosableFileFilter(this.xmlFilter);
		this.fileChooser.removeChoosableFileFilter(this.cssFilter);
		final int returnVal = this.fileChooser.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.brutCopyDir = this.fileChooser.getSelectedFile();
			this.jBrutCopyTextField.setText(this.brutCopyDir.getAbsolutePath());
		}
	}

	private void processExportDirButton() {
		final String dir = this.jExportDirTextField.getText();
		if (dir != null) {
			this.fileChooser = new JFileChooser(new File(dir));
			this.fileChooser.setAcceptAllFileFilterUsed(false);
		}
		this.fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		this.fileChooser.removeChoosableFileFilter(this.xslFilter);
		this.fileChooser.removeChoosableFileFilter(this.xmlFilter);
		this.fileChooser.removeChoosableFileFilter(this.cssFilter);
		final int returnVal = this.fileChooser.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.exportDir = this.fileChooser.getSelectedFile();
			this.jExportDirTextField.setText(this.exportDir.getAbsolutePath());
		}
	}

	private void processCSSFileButton() {
		final String dir = this.jCSSTextField.getText();
		if (dir != null) {
			this.fileChooser = new JFileChooser(new File(dir));
			this.fileChooser.setAcceptAllFileFilterUsed(false);
		}
		this.fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		this.fileChooser.addChoosableFileFilter(this.cssFilter);
		this.fileChooser.removeChoosableFileFilter(this.xmlFilter);
		this.fileChooser.removeChoosableFileFilter(this.xslFilter);
		final int returnVal = this.fileChooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.cssFile = this.fileChooser.getSelectedFile();
			this.jCSSTextField.setText(this.cssFile.getAbsolutePath());
		}
	}

	private void processXSLPlatformFileButton() {
		final String dir = this.jXSLPlatformTextField.getText();
		if (dir != null) {
			this.fileChooser = new JFileChooser(new File(dir));
			this.fileChooser.setAcceptAllFileFilterUsed(false);
		}
		this.fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		this.fileChooser.addChoosableFileFilter(this.xslFilter);
		this.fileChooser.removeChoosableFileFilter(this.xmlFilter);
		this.fileChooser.removeChoosableFileFilter(this.cssFilter);
		final int returnVal = this.fileChooser.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.xslPlatformFile = this.fileChooser.getSelectedFile();
			this.jXSLPlatformTextField.setText(this.xslPlatformFile.getAbsolutePath());
		}
	}

	private void processXSLDetailsFileButton() {
		final String dir = this.jXSLDetailsTextField.getText();
		if (dir != null) {
			this.fileChooser = new JFileChooser(new File(dir));
			this.fileChooser.setAcceptAllFileFilterUsed(false);
		}
		this.fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		this.fileChooser.addChoosableFileFilter(this.xslFilter);
		this.fileChooser.removeChoosableFileFilter(this.xmlFilter);
		this.fileChooser.removeChoosableFileFilter(this.cssFilter);
		final int returnVal = this.fileChooser.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.xslDetailsFile = this.fileChooser.getSelectedFile();
			this.jXSLDetailsTextField.setText(this.xslDetailsFile.getAbsolutePath());
		}
	}

	private void processXSLIndexFileButton() {
		final String dir = this.jXSLIndexTextField.getText();
		if (dir != null) {
			this.fileChooser = new JFileChooser(new File(dir));
			this.fileChooser.setAcceptAllFileFilterUsed(false);
		}
		this.fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		this.fileChooser.addChoosableFileFilter(this.xslFilter);
		this.fileChooser.removeChoosableFileFilter(this.xmlFilter);
		this.fileChooser.removeChoosableFileFilter(this.cssFilter);
		final int returnVal = this.fileChooser.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.xslIndexFile = this.fileChooser.getSelectedFile();
			this.jXSLIndexTextField.setText(this.xslIndexFile.getAbsolutePath());
		}
	}

	private void processXMLFileButton() {
		final String dir = this.jXMLTextField.getText();
		if (dir != null) {
			this.fileChooser = new JFileChooser(new File(dir));
			this.fileChooser.setAcceptAllFileFilterUsed(false);
		}
		this.fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		this.fileChooser.addChoosableFileFilter(this.xmlFilter);
		this.fileChooser.removeChoosableFileFilter(this.xslFilter);
		this.fileChooser.removeChoosableFileFilter(this.cssFilter);
		final int returnVal = this.fileChooser.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.xmlFile = this.fileChooser.getSelectedFile();
			this.jXMLTextField.setText(this.xmlFile.getAbsolutePath());
		}
	}

	@NonNull
	private ImageContext getImageContext() {

		return new ImageContext(this.jCreateThumbnailImage.isSelected(), Integer.valueOf(this.thumbnailWidth.getText()).intValue(), this.jExportImage.isSelected(), "");

	}

	@NonNull
	private HTMLFileContext getHtmlIndexContext() {
		final HTMLFileContext context = new HTMLFileContext();
		context.setCompressHTML(this.compressHTML.isSelected());
		context.setDestDir(this.exportDir);
		context.setSortByTitle(this.jsortByTitle.isSelected());
		context.setSplitEvery(Integer.valueOf(this.splitIndexEvery.getText()));
		context.setXmlFile(this.xmlFile);
		context.setXslDetailsFile(this.xslDetailsFile);
		context.setXslIndexFile(this.xslIndexFile);
		if (this.splitByPlatform != null) {
			context.setSplitByPlatform(this.splitByPlatform.isSelected());
			context.setXslPlatform(this.xslPlatformFile);
		}

		return context;
	}

	@Override
	public void itemStateChanged(final ItemEvent e) {
		final Object component = e.getSource();
		if (component == this.jCreateThumbnailImage) {
			this.thumbnailPanel.setVisible(this.jCreateThumbnailImage.isSelected());
		} else if (component instanceof JComboBox<?>) {
			final int state = e.getStateChange();
			if (state == ItemEvent.SELECTED) {
				final ItemSelectable is = e.getItemSelectable();
				final Object selected[] = is.getSelectedObjects();
				if (selected.length > 0) {
					this.exportType = ExportType.getExportType((String) selected[0]);
					if (this.exportType != null) {
						processChangeExportType();
					}
				}
			}
		}
	}

	private void processChangeExportType() {
		// clean screen
		final Component[] components = getComponents();
		for (final Component component : components) {
			remove(component);
		}

		int currentRow = addCommonComponents(0);

		currentRow++;
		this.exportButton = new JButton("Export!");
		this.exportButton.addActionListener(this);
		add(this.exportButton, new GridBagConstraints(0, currentRow, 3, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 10, 0, 10), 1, 1));

		revalidate();
	}

	private int addCommonComponents(final int row) {
		final PropertyPreference preference = this.exportType.getPropertyPreference();
		final Map<String, @NonNull String> propertiesConstants = preference.getValues();

		int currentRow = row;
		// Title
		final JLabel titleLabel = new JLabel("Export " + this.exportType.getName());
		final Font font = titleLabel.getFont();
		final Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize() + 10);
		titleLabel.setFont(boldFont);
		add(titleLabel, new GridBagConstraints(0, currentRow, 4, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		currentRow++;
		// XML File
		final JLabel label = new JLabel("XML File ");
		label.setToolTipText("Set the path to the XML data file.");
		add(label, new GridBagConstraints(0, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.jXMLTextField = new JTextField(30);
		this.jXMLTextField.setToolTipText("Set the path to the XML data file.");
		String setting = this.preferences.get(propertiesConstants.get(PropertyConstants.XML_FILE), null);
		if (setting != null) {
			this.jXMLTextField.setText(setting);
			this.xmlFile = new File(setting);
		}
		this.jXMLTextField.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.XML_FILE)));
		add(this.jXMLTextField, new GridBagConstraints(1, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.xmlFileButton = new JButton("Select File");
		this.xmlFileButton.addActionListener(this);
		add(this.xmlFileButton, new GridBagConstraints(2, currentRow, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		currentRow++;
		// XSL Index File
		final JLabel label1 = new JLabel("XSL Index File ");
		label1.setToolTipText("Set the path to the XSL file used for generate HTML index page(s)");
		add(label1, new GridBagConstraints(0, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.jXSLIndexTextField = new JTextField(30);
		this.jXSLIndexTextField.setToolTipText("Set the path to the XSL file used for generate HTML index page(s)");
		setting = this.preferences.get(propertiesConstants.get(PropertyConstants.XSL_INDEX), null);
		if (setting != null) {
			this.jXSLIndexTextField.setText(setting);
			this.xslIndexFile = new File(setting);
		}
		this.jXSLIndexTextField.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.XSL_INDEX)));
		add(this.jXSLIndexTextField, new GridBagConstraints(1, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.xslIndexFileButton = new JButton("Select File");
		this.xslIndexFileButton.addActionListener(this);
		add(this.xslIndexFileButton, new GridBagConstraints(2, currentRow, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		currentRow++;
		// XSL Details File
		final JLabel label2 = new JLabel("XSL Details File ");
		label2.setToolTipText("Set the path to the XSL file used for generate HTML detail page(s)");
		add(label2, new GridBagConstraints(0, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.jXSLDetailsTextField = new JTextField(30);
		this.jXSLDetailsTextField.setToolTipText("Set the path to the XSL file used for generate HTML detail page(s)");
		setting = this.preferences.get(propertiesConstants.get(PropertyConstants.XSL_DETAILS), null);
		if (setting != null) {
			this.jXSLDetailsTextField.setText(setting);
			this.xslDetailsFile = new File(setting);
		}
		this.jXSLDetailsTextField.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.XSL_DETAILS)));
		add(this.jXSLDetailsTextField, new GridBagConstraints(1, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.xslDetailsFileButton = new JButton("Select File");
		this.xslDetailsFileButton.addActionListener(this);
		add(this.xslDetailsFileButton, new GridBagConstraints(2, currentRow, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		currentRow++;
		// CSS File
		final JLabel cssLabel = new JLabel("<html>CSS File<br> (leave empty if not needed) </html>");
		cssLabel.setToolTipText("Set CSS file which will be copied at the root of the destination directory");
		add(cssLabel, new GridBagConstraints(0, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.jCSSTextField = new JTextField(30);
		this.jCSSTextField.setToolTipText("Set CSS file which will be copied at the root of the destination directory");
		setting = this.preferences.get(propertiesConstants.get(PropertyConstants.CSS_FILE), null);
		if (setting != null) {
			this.jCSSTextField.setText(setting);
			this.cssFile = new File(setting);
		}
		this.jCSSTextField.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.CSS_FILE)));
		add(this.jCSSTextField, new GridBagConstraints(1, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.cssFileButton = new JButton("Select File");
		this.cssFileButton.addActionListener(this);
		add(this.cssFileButton, new GridBagConstraints(2, currentRow, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		currentRow++;
		// Export dir
		final JLabel exportDirLabel = new JLabel("Export to directory ");
		exportDirLabel.setToolTipText("Set the destination directory for export file(s)");
		add(exportDirLabel, new GridBagConstraints(0, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.jExportDirTextField = new JTextField(30);
		this.jExportDirTextField.setToolTipText("Set the destination directory for export file(s)");
		setting = this.preferences.get(propertiesConstants.get(PropertyConstants.HTML_DIR), null);
		if (setting != null) {
			this.jExportDirTextField.setText(setting);
			this.exportDir = new File(setting);
		}
		this.jExportDirTextField.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.HTML_DIR)));
		add(this.jExportDirTextField, new GridBagConstraints(1, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.exportDirButton = new JButton("Select Directory");
		this.exportDirButton.addActionListener(this);
		add(this.exportDirButton, new GridBagConstraints(2, currentRow, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		currentRow++;
		// brut copy dir
		final JLabel brutCopy = new JLabel("<html>Brut copy files<br> (leave empty if not needed) </html>");
		brutCopy.setToolTipText("Set directory that will be copied (sub-directories included)");
		add(brutCopy, new GridBagConstraints(0, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.jBrutCopyTextField = new JTextField(30);
		this.jBrutCopyTextField.setToolTipText("Set directory that will be copied (sub-directories included)");
		setting = this.preferences.get(propertiesConstants.get(PropertyConstants.BRUT_COPY), null);
		if (setting != null) {
			this.jBrutCopyTextField.setText(setting);
			this.brutCopyDir = new File(setting);
		}
		this.jBrutCopyTextField.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.BRUT_COPY)));
		add(this.jBrutCopyTextField, new GridBagConstraints(1, currentRow, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		this.brutCopyDirButton = new JButton("Select Directory");
		this.brutCopyDirButton.addActionListener(this);
		add(this.brutCopyDirButton, new GridBagConstraints(2, currentRow, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		currentRow++;
		final JPanel panels = new JPanel(new GridBagLayout());
		panels.setBackground(Color.WHITE);
		add(panels, new GridBagConstraints(0, currentRow, 4, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 10, 0, 0), 1, 1));
		final JPanel leftPanel = getLeftPanel(propertiesConstants);
		panels.add(leftPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		final JPanel rightPanel = getRightPanel(propertiesConstants);
		panels.add(rightPanel, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		return currentRow;
	}

	@NonNull
	private JPanel getRightPanel(final Map<String, @NonNull String> propertiesConstants) {
		final JPanel panel = new JPanel(new GridBagLayout());
		panel.setBackground(Color.WHITE);
		// Compress CSS file(s)
		this.compressHTML = new JCheckBox("Compress HTML file(s)", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.COMPRESS_HTML), true));
		this.compressHTML.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.COMPRESS_HTML)));
		this.compressHTML.setBackground(Color.WHITE);
		panel.add(this.compressHTML, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		// Compress CSS file(s)
		this.compressCSS = new JCheckBox("Compress CSS file(s)", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.COMPRESS_CSS), true));
		this.compressCSS.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.COMPRESS_CSS)));
		this.compressCSS.setBackground(Color.WHITE);
		panel.add(this.compressCSS, new GridBagConstraints(0, 1, 3, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		// Obfuscate javascript file(s)
		this.obfuscateJS = new JCheckBox("Obfuscate/Minimize Javascript file(s)", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.COMPRESS_CSS), true));
		this.obfuscateJS.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.COMPRESS_CSS)));
		this.obfuscateJS.setBackground(Color.WHITE);
		panel.add(this.obfuscateJS, new GridBagConstraints(0, 2, 3, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		// Copy only new or size different file(s)
		this.copyNewDifferentSize = new JCheckBox("Copy new files or files of different sizes", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.COPY_NEW_DIFFERENT_SIZE), true));
		this.copyNewDifferentSize.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.COPY_NEW_DIFFERENT_SIZE)));
		this.copyNewDifferentSize.setBackground(Color.WHITE);
		panel.add(this.copyNewDifferentSize, new GridBagConstraints(0, 3, 3, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		if (this.exportType == ExportType.GAME) {
			// Split by platform
			this.splitByPlatform = new JCheckBox("Split by platform", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.SPLIT_BY_PLATFORM), true));
			this.splitByPlatform.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.SPLIT_BY_PLATFORM)));
			this.splitByPlatform.setBackground(Color.WHITE);
			panel.add(this.splitByPlatform, new GridBagConstraints(0, 4, 3, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
			// XSL cat File
			final JLabel label2 = new JLabel("XSL Platforms File ");
			label2.setToolTipText("Set the path to the XSL file used for generate HTML platform page(s)");
			panel.add(label2, new GridBagConstraints(0, 5, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
			this.jXSLPlatformTextField = new JTextField(30);
			this.jXSLPlatformTextField.setToolTipText("Set the path to the XSL file used for generate HTML platform page(s)");
			final String setting = this.preferences.get(propertiesConstants.get(PropertyConstants.XSL_PLATFORMS), null);
			if (setting != null) {
				this.jXSLPlatformTextField.setText(setting);
				this.xslPlatformFile = new File(setting);
			}
			this.jXSLPlatformTextField.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.XSL_PLATFORMS)));
			panel.add(this.jXSLPlatformTextField, new GridBagConstraints(1, 5, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
			this.xslPlatformFileButton = new JButton("Select File");
			this.xslPlatformFileButton.addActionListener(this);
			panel.add(this.xslPlatformFileButton, new GridBagConstraints(2, 5, 1, 1, 0, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		}

		return panel;
	}

	@NonNull
	private JPanel getLeftPanel(final Map<String, @NonNull String> propertiesConstants) {
		final JPanel panel = new JPanel(new GridBagLayout());
		panel.setBackground(Color.WHITE);
		// Sort item(s) by title
		this.jsortByTitle = new JCheckBox("Sort item by title", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.SORT_BY_TITLE), true));
		this.jsortByTitle.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.SORT_BY_TITLE)));
		this.jsortByTitle.setBackground(Color.WHITE);
		panel.add(this.jsortByTitle, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		// Export image(s)
		this.jExportImage = new JCheckBox("Export Image(s)", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.IMAGE), true));
		this.jExportImage.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.IMAGE)));
		this.jExportImage.setBackground(Color.WHITE);
		this.jExportImage.addItemListener(this);
		panel.add(this.jExportImage, new GridBagConstraints(0, 1, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		// Create thumbnail
		final boolean createThumbnail = this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.THUMBNAIL), true);
		this.jCreateThumbnailImage = new JCheckBox("Create Thumbnail Image(s)", createThumbnail);
		this.jCreateThumbnailImage.setBackground(Color.WHITE);
		this.jCreateThumbnailImage.addItemListener(this);
		this.jCreateThumbnailImage.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.THUMBNAIL)));
		panel.add(this.jCreateThumbnailImage, new GridBagConstraints(0, 2, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));

		// Thumbnail size
		this.thumbnailPanel = new JPanel();
		this.thumbnailPanel.setLayout(new GridBagLayout());
		this.thumbnailPanel.setBackground(Color.WHITE);
		final JLabel thumbnailLabel = new JLabel("Thumbnail width size (in px)");
		this.thumbnailPanel.add(thumbnailLabel, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 1, 1));
		final NumberFormat integerFormat = NumberFormat.getNumberInstance();
		integerFormat.setParseIntegerOnly(true);
		this.thumbnailWidth = new JFormattedTextField(integerFormat);
		this.thumbnailWidth.setColumns(4);
		this.thumbnailWidth.setHorizontalAlignment(SwingConstants.RIGHT);
		this.thumbnailWidth.setText(String.valueOf(this.preferences.getInt(propertiesConstants.get(PropertyConstants.THUMBNAIL_SIZE), 100)));
		this.thumbnailWidth.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.THUMBNAIL_SIZE)));
		this.thumbnailPanel.add(this.thumbnailWidth, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 1, 1));
		this.thumbnailPanel.setVisible(createThumbnail);
		panel.add(this.thumbnailPanel, new GridBagConstraints(0, 3, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 30, 0, 0), 1, 1));

		// Split every x pages
		final JPanel splitEveryJPanel = new JPanel();
		splitEveryJPanel.setBackground(Color.WHITE);
		final JLabel splitEveryJLabel = new JLabel("Split page every x items (set 0 for no split)");
		splitEveryJPanel.add(splitEveryJLabel, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 1, 1));
		this.splitIndexEvery = new JFormattedTextField(integerFormat);
		this.splitIndexEvery.setColumns(2);
		this.splitIndexEvery.setHorizontalAlignment(SwingConstants.RIGHT);
		this.splitIndexEvery.setText(String.valueOf(this.preferences.getInt(propertiesConstants.get(PropertyConstants.SPLIT_EVERY), 0)));
		this.splitIndexEvery.getDocument().addDocumentListener(new TextListener(this.preferences, propertiesConstants.get(PropertyConstants.SPLIT_EVERY)));
		splitEveryJPanel.add(this.splitIndexEvery, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 1, 1));
		panel.add(splitEveryJPanel, new GridBagConstraints(0, 4, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 1, 1));
		if (this.exportType == ExportType.MUSIC) {
			// Export media file(s)
			this.jExportMP3CheckBox = new JCheckBox("Copy MP3/FLV file(s) referenced in XML", this.preferences.getBoolean(propertiesConstants.get(PropertyConstants.MEDIA), true));
			this.jExportMP3CheckBox.setBackground(Color.WHITE);
			this.jExportMP3CheckBox.addItemListener(new CheckBoxListener(this.preferences, propertiesConstants.get(PropertyConstants.MEDIA)));
			panel.add(this.jExportMP3CheckBox, new GridBagConstraints(0, 5, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 1, 1));
		}

		return panel;
	}

	/**
	 * Add ExportEventListener.
	 *
	 * @param listener
	 */
	public void addMessageHandler(@NonNull final ExportEventListener listener) {
		this.exportEventListener = listener;
	}
}
