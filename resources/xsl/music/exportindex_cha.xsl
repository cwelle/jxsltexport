﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:include href="../common_cha.xsl"/>
<xsl:output method="html"/>

<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css"/>
<link href="csscha.css" rel="stylesheet" media="screen" type="text/css" />
<link href="cssmusic.css" rel="stylesheet" media="screen" type="text/css" />
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="./script/jquery.bighover.js" type="text/javascript"></script>
<script src="./script/scripti.js" type="text/javascript"></script>
<link href="./images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body class="music app music page-view">
<div class="wrap">
	<div id="content-data-wide" class="listview">
	    <div id="view-main">
			<div id="view-content" class="list">
			    <xsl:apply-templates select="musicinfo/musiclist"/>
			    <xsl:apply-templates select="musicinfo/navigation"/> 
    		</div>   
		</div>   
	</div>
</div>
</body>
</html>
</xsl:template>

<xsl:template match="musiclist">
   <table class="table table-hover table-bordered table-striped table-condensed x-collection">
      <thead>
      	<tr>
      		<th class="item-incollection"></th>
      		<th class='item-title'>Titre</th>
      		<th class='item-artist'>Artiste(s)</th>
      		<th class='item-genre'>Genre(s)</th>
      		<th class='item-discs'>Disque(s)</th>
      		<th class='item-tracks'>Piste(s)</th>
      		<th class='item-releasedate'>Date de sortie</th>
      		<th class='item-releasedate'>Support</th>
      		<th class='item-label'>Label</th>
      		<th class='item-rating'>Ma note</th>
      		<th class='item-added'>Date d'ajout</th>     
      	</tr> 
      </thead>
      <tbody>
		<xsl:for-each select="music">
		<tr class="item" onclick="document.location = 'details/{id}.html';" style="cursor: pointer">
			<td class="item-incollection" width="100px" valign="top">
				<a href="details/{id}.html" class="title-detail">
					<xsl:choose>
						<xsl:when test="thumbfilepath!=''">
							<img class="block-thumbnail" src="images/{id}t.jpg"/>
						</xsl:when>
						<xsl:otherwise>
							<img class="block-thumbnail" src="images/mainitem.jpg"/>
						</xsl:otherwise>
					</xsl:choose>
				</a>
			</td>
			<td class="item-artist" valign="top" title="Titre">
		    	<a href="details/{id}.html" class="title-detail"><xsl:value-of select="title"/></a>
		    </td><td class="item-title" valign="top" title="Artiste(s)">
				<xsl:for-each select="artists/artist">
					<xsl:call-template name="listItem">
	                    <xsl:with-param name="features" select="displayname"/>
	                </xsl:call-template>
		    	</xsl:for-each>
		    </td>
			<td class="item-genre" width="120px" valign="top" title="Genre(s)">
				<xsl:for-each select="genres/genre">
		      		<xsl:if test="position() > 1"><BR/></xsl:if>
		      		<xsl:value-of select="displayname" /> 
		    	</xsl:for-each>
			</td>
			<td class="item-discs" width="80px" valign="top" title="Disque(s)"><xsl:value-of select="nrdiscs"/></td>
			<td class="item-tracks" width="70px" valign="top" title="Piste(s)"><xsl:value-of select="nrtracks"/></td>
			<td class="item-releasedate" valign="top" title="Date de sortie"><xsl:call-template name="formatDate2"><xsl:with-param name="value" select="releasedate/date"/></xsl:call-template></td>
			<td class="item-releasedate" width="80px" title="Support" valign="top"><xsl:value-of select="format/displayname"/></td>
			<td class="item-label" valign="top" title="Label"><xsl:value-of select="label/displayname"/></td>
			<td class="item-rating" valign="top" title="Note" width="175px">
				<xsl:call-template name="star">
					<xsl:with-param name="num" select="rating"/>
					<xsl:with-param name="relativePath" select="'./'"/>
				</xsl:call-template> 
			</td>
			<td class="item-added" valign="top" title="Date d'ajout">
				<xsl:call-template name="formatDate"><xsl:with-param name="value" select="dateadded/date"/></xsl:call-template> 
			</td></tr>    
		</xsl:for-each>
		</tbody>
	</table>
</xsl:template> 
</xsl:stylesheet>
