﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="formatDate">
	<xsl:param name="value"/>
    <xsl:variable name="date" select="substring-before($value, ' ')"/>
    <xsl:variable name="D" select="substring-before($date, '/')"/>
    <xsl:variable name="M-Y" select="substring-after($date, '/')"/>
    <xsl:variable name="M" select="substring-before($M-Y, '/')"/>
    <xsl:variable name="Y" select="substring-after($M-Y, '/')"/>

    <xsl:variable name="nameMonth">
	    <xsl:choose>
		  <xsl:when test="$M = '1'">
		  	Janvier
		  </xsl:when>
		  <xsl:when test="$M = '2'">
		  	Février
		  </xsl:when>
		  <xsl:when test="$M = '3'">
		  	Mars
		  </xsl:when>
		  <xsl:when test="$M = '4'">
		  	Avril
		  </xsl:when>
		  <xsl:when test="$M = '5'">
		  	Mai
		  </xsl:when>
		  <xsl:when test="$M = '6'">
		  	Juin
		  </xsl:when>
		  <xsl:when test="$M = '7'">
		  	Juillet
		  </xsl:when>
		  <xsl:when test="$M = '8'">
		  	Août
		  </xsl:when>
		  <xsl:when test="$M = '9'">
		  	Septembre
		  </xsl:when>
		  <xsl:when test="$M = '10'">
		  	Octobre
		  </xsl:when>
		  <xsl:when test="$M = '11'">
		  	Novembre
		  </xsl:when>
		  <xsl:when test="$M = '12'">
		  	Décembre
		  </xsl:when>
		</xsl:choose>
    </xsl:variable>
    
    <xsl:value-of select="concat($D, ' ', $nameMonth, ' ',  $Y)"/>
</xsl:template>

<!-- Determine File Extention template -->
<xsl:template name="get-file-extension">
    <xsl:param name="path"/>
    <xsl:choose>
        <xsl:when test="contains($path, '/')">
            <xsl:call-template name="get-file-extension">
                <xsl:with-param name="path" select="substring-after($path, '/')"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:when test="contains($path, '.')">
            <xsl:call-template name="get-file-extension">
                <xsl:with-param name="path" select="substring-after($path, '.')"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$path"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="formatDate2">
	<xsl:param name="value"/>
    <xsl:variable name="D" select="substring-before($value, '/')"/>
    <xsl:variable name="M-Y" select="substring-after($value, '/')"/>
    <xsl:variable name="M" select="substring-before($M-Y, '/')"/>
    <xsl:variable name="Y" select="substring-after($M-Y, '/')"/>

    <xsl:variable name="nameMonth">
	    <xsl:choose>
		  <xsl:when test="$M = '1' or $M = '01'">
		  	Janvier
		  </xsl:when>
		  <xsl:when test="$M = '2' or $M = '02'">
		  	Février
		  </xsl:when>
		  <xsl:when test="$M = '3' or $M = '03'">
		  	Mars
		  </xsl:when>
		  <xsl:when test="$M = '4' or $M = '04'">
		  	Avril
		  </xsl:when>
		  <xsl:when test="$M = '5' or $M = '05'">
		  	Mai
		  </xsl:when>
		  <xsl:when test="$M = '6' or $M = '06'">
		  	Juin
		  </xsl:when>
		  <xsl:when test="$M = '7' or $M = '07'">
		  	Juillet
		  </xsl:when>
		  <xsl:when test="$M = '8' or $M = '08'">
		  	Août
		  </xsl:when>
		  <xsl:when test="$M = '9' or $M = '09'">
		  	Septembre
		  </xsl:when>
		  <xsl:when test="$M = '10'">
		  	Octobre
		  </xsl:when>
		  <xsl:when test="$M = '11'">
		  	Novembre
		  </xsl:when>
		  <xsl:when test="$M = '12'">
		  	Décembre
		  </xsl:when>
		</xsl:choose>
    </xsl:variable>
    
    <xsl:value-of select="concat($D, ' ', $nameMonth, ' ',  $Y)"/>
</xsl:template>

<xsl:template name="star">
	<xsl:param name="num" select="0"/>
	<xsl:param name="relativePath" select="."/>
	<xsl:variable name="rating">
		<xsl:choose>
		   <xsl:when test="contains($num, '%')">
					<xsl:variable name="percentage">			   
		         <xsl:value-of select="substring-before($num,'%')"/>
					</xsl:variable>
					<xsl:value-of select="$percentage div 10"/>
		   </xsl:when>
		   <xsl:otherwise>		
		       <xsl:value-of select="$num"/>
		   </xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:call-template name="displayStars">
         <xsl:with-param name="i" select="'0'" />
         <xsl:with-param name="length" select="$rating" />
         <xsl:with-param name="image" select="concat($relativePath, '/images/StarFull.png')" />
       </xsl:call-template>
   	<xsl:call-template name="displayStars">
         <xsl:with-param name="i" select="$rating + 1" />
         <xsl:with-param name="length" select="'11'" />
         <xsl:with-param name="image" select="concat($relativePath, '/images/StarEmpty.png')" />
    </xsl:call-template> 
</xsl:template>

<xsl:template name="displayStars">
  <xsl:param name="i" />
  <xsl:param name="length" />
  <xsl:param name="image" />

  <!-- Tant que length n'est pas atteint, l'on rappele le template pour continuer la boucle -->
  <xsl:if test="$i &lt; $length">
	<!-- Traitement du for -->
	<img src="{$image}" width="16px" height="16px"/>
	<!-- /Traitement du for -->
    <xsl:call-template name="displayStars">
      <xsl:with-param name="i" select="$i + 1" />
      <xsl:with-param name="length" select="$length" />
      <xsl:with-param name="image" select="$image" />
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<xsl:template name="substring-after-last">
    <xsl:param name="string"/>
    <xsl:param name="char"/>

    <xsl:choose>
      <xsl:when test="contains($string, $char)">
        <xsl:call-template name="substring-after-last">
          <xsl:with-param name="string" select="substring-after($string, $char)"/>
          <xsl:with-param name="char" select="$char"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="youtube-id">
    <xsl:param name="string"/>

	<xsl:if test="contains($string, '=')">
		<xsl:value-of select="substring-after($string, '=')"/>
	</xsl:if>
</xsl:template>

<xsl:template match="links">
	<xsl:param name="relativePath" select="."/>
	<xsl:variable name="image">
		<xsl:value-of select="count(//link[urltype = 'Image'])"/>
	</xsl:variable>
	
	<xsl:if test="$image > 0">
		<div class="item-image">
			<h2 class="darkColor">Screenshots</h2>
			<xsl:for-each select="link">
				<xsl:variable name="filename">
					<xsl:call-template name="substring-after-last">
				      <xsl:with-param name="string" select="url"/>
				      <xsl:with-param name="char" select="'\'"/>
				    </xsl:call-template>
				</xsl:variable>		
		  		<xsl:if test="urltype = 'Image'">
		  			<xsl:variable name="urlfile">
		  				<xsl:value-of select="concat($relativePath, '/images/', $filename)"></xsl:value-of>
		  			</xsl:variable>
		  			<img src="{$urlfile}" class="item-frontcover lightbox"/>
		  		</xsl:if>
			</xsl:for-each>
		</div>
	</xsl:if>
</xsl:template>

<xsl:template match="navigation">
  <div class="navigation" align="center">
    <xsl:if test="count(pagelink) > 1">
      <div class="navigationline">
        <xsl:choose>
          <xsl:when test="firstlink/@url!=''">
            <a href="{firstlink/@url}">Premier</a>&#160;
          </xsl:when>
        </xsl:choose>   
        <xsl:choose>
          <xsl:when test="prevlink/@url!=''">
            <a href="{prevlink/@url}">Précédent</a>&#160;
          </xsl:when>
        </xsl:choose>   
        <xsl:for-each select="pagelink">
          <xsl:choose>
            <xsl:when test="@url!=''">      
              <a href="{@url}"><xsl:value-of select="@pagenum"/></a>&#160;
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@pagenum"/>&#160;
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="position()!=last()"></xsl:if>
        </xsl:for-each>  
        <xsl:choose>
          <xsl:when test="nextlink/@url!=''">
            <a href="{nextlink/@url}">Suivant</a>&#160;
          </xsl:when>
        </xsl:choose>   
        <xsl:choose>
          <xsl:when test="lastlink/@url!=''">
            <a href="{lastlink/@url}">Dernier</a>&#160;
          </xsl:when>
        </xsl:choose> 
      </div>
    </xsl:if>
  </div>
</xsl:template>

<xsl:template name="listItem">
    <xsl:param name="features"/>
    <xsl:param name="delimiter" select="';'"/>
    <xsl:param name="delimiter2" select="','"/>
    <xsl:param name="delimiter3" select="'&amp;'"/>
    <xsl:choose>
        <xsl:when test="contains($features, $delimiter)">
            <xsl:value-of select="normalize-space(substring-before($features, $delimiter))"/><BR/>
            <xsl:variable name="nextValue" select="substring-after($features, $delimiter)"/>
            <xsl:if test="normalize-space($nextValue)">
                <xsl:call-template name="listItem">
                    <xsl:with-param name="features" select="$nextValue"/>
                    <xsl:with-param name="delimiter" select="$delimiter"/>
                </xsl:call-template>    
            </xsl:if>
        </xsl:when>
    	<xsl:when test="contains($features, $delimiter2)">
            <xsl:value-of select="normalize-space(substring-before($features, $delimiter2))"/><BR/>
            <xsl:variable name="nextValue" select="substring-after($features, $delimiter2)"/>
            <xsl:if test="normalize-space($nextValue)">
                <xsl:call-template name="listItem">
                    <xsl:with-param name="features" select="$nextValue"/>
                    <xsl:with-param name="delimiter" select="$delimiter2"/>
                </xsl:call-template>    
            </xsl:if>
        </xsl:when>
    	<xsl:when test="contains($features, $delimiter3)">
            <xsl:value-of select="normalize-space(substring-before($features, $delimiter3))"/><BR/>
            <xsl:variable name="nextValue" select="substring-after($features, $delimiter3)"/>
            <xsl:if test="normalize-space($nextValue)">
                <xsl:call-template name="listItem">
                    <xsl:with-param name="features" select="$nextValue"/>
                    <xsl:with-param name="delimiter" select="$delimiter3"/>
                </xsl:call-template>    
            </xsl:if>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$features"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>

