package aka.xlstexport.main.helpers;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.annotation.NonNull;

/**
 * File(s) helper.
 *
 * @author Akazukin
 */
public class FilesHelper {

	/**
	 * Check if given newFile is newer or have a different size than the given
	 * oldFile.
	 *
	 * @param oldFile
	 * @param newFile
	 * @return <code>true</code> if newFile is newer or have a different size
	 */
	public static boolean isNewOrDifferentSize(@NonNull final File oldFile, @NonNull final File newFile) {
		boolean result = true;
		if (oldFile.exists()) {
			result = FileUtils.isFileNewer(newFile, oldFile);
			if (!result) {
				result = oldFile.length() != newFile.length();
			}
		}
		return result;
	}

	/**
	 * Get the content of the given file.
	 *
	 * @param file
	 * @return string content
	 * @throws IOException
	 */
	public static String readFile(@NonNull final File file) throws IOException {
		final byte[] encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
		return new String(encoded, StandardCharsets.ISO_8859_1);
	}

	private FilesHelper() {
		// Singleton
	}
}
