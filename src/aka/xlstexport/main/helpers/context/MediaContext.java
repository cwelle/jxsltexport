package aka.xlstexport.main.helpers.context;

/**
 * Media file(s) context.
 *
 * @author Akazukin
 */
public class MediaContext {

	private final boolean copyMedia;

	/**
	 * Constructor.
	 *
	 * @param copyMediaParam
	 */
	public MediaContext(final boolean copyMediaParam) {
		this.copyMedia = copyMediaParam;
	}

	/**
	 * @return the copyMedia
	 */
	public boolean copyMedia() {
		return this.copyMedia;
	}

}
