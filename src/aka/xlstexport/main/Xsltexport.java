package aka.xlstexport.main;

import java.util.prefs.Preferences;

import aka.xlstexport.main.config.PropertyPreferencesFactory;

/**
 * Main class.
 *
 * @author Akazukin
 */
public class Xsltexport {

	/**
	 * Main.
	 */
	public static void main(@SuppressWarnings("javadoc") final String[] args) {
		System.setProperty("java.util.prefs.PreferencesFactory", PropertyPreferencesFactory.class.getName());
		System.setProperty(PropertyPreferencesFactory.SYSTEM_PROPERTY_FILE, "myprefs.txt");

		final Preferences p = Preferences.userNodeForPackage(Xsltexport.class);
		assert p != null;
		new MainWindow(p).setVisible(true);
	}
}
