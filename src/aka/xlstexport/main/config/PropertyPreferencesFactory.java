package aka.xlstexport.main.config;

import java.io.File;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

public class PropertyPreferencesFactory implements PreferencesFactory {

	private static final Logger log = Logger.getLogger(PropertyPreferencesFactory.class.getName());

	private static File preferencesFile;
	Preferences rootPreferences;
	public static final String SYSTEM_PROPERTY_FILE = "aka.xlstexport.main.config.PropertyPreferencesFactory.file";

	@Override
	public Preferences systemRoot() {
		return userRoot();
	}

	@Override
	public Preferences userRoot() {
		if (this.rootPreferences == null) {
			log.finer("Instantiating root preferences");

			this.rootPreferences = new PropertyPreferences(null, "");
		}
		return this.rootPreferences;
	}

	public static File getPreferencesFile() {
		if (preferencesFile == null) {
			String prefsFile = System.getProperty(SYSTEM_PROPERTY_FILE);
			if (prefsFile == null || prefsFile.length() == 0) {
				prefsFile = System.getProperty("user.home") + File.separator + ".fileprefs";
			}
			preferencesFile = new File(prefsFile).getAbsoluteFile();
			log.finer("Preferences file is " + preferencesFile);
		}
		return preferencesFile;
	}
}
