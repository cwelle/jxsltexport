package aka.xlstexport.main.obfuscators;

import org.eclipse.jdt.annotation.NonNull;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

/**
 * JS report error message useable in JavascriptCompressor.
 *
 * @author Akazukin
 */
public class JSErrorReporter implements ErrorReporter {

	@NonNull
	private final String localFilename;

	/**
	 * Constructor.
	 *
	 * @param fileName
	 */
	public JSErrorReporter(@NonNull final String fileName) {
		this.localFilename = fileName;
	}

	@Override
	public void warning(final String message, final String sourceName, final int line, final String lineSource, final int lineOffset) {
		final StringBuilder sb = new StringBuilder();
		sb.append("[WARNING] in " + this.localFilename);
		if (line < 0) {
			sb.append("  " + message);
		} else {
			sb.append("  at line " + line + " columns " + lineOffset + " : " + message);
		}
		// JOptionPane.showMessageDialog(null, sb.toString());
	}

	@Override
	public void error(final String message, final String sourceName, final int line, final String lineSource, final int lineOffset) {
		final StringBuilder sb = new StringBuilder();
		sb.append("[ERROR] in " + this.localFilename);
		if (line < 0) {
			sb.append("  " + message);
		} else {
			sb.append("  at line " + line + " columns " + lineOffset + " : " + message);
		}
		// JOptionPane.showMessageDialog(null, sb.toString());
	}

	@Override
	public EvaluatorException runtimeError(final String message, final String sourceName, final int line, final String lineSource, final int lineOffset) {
		error(message, sourceName, line, lineSource, lineOffset);
		return new EvaluatorException(message);
	}
}
