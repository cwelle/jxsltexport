package aka.xlstexport.main.config;

import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;

import aka.xlstexport.main.constants.PropertyConstants;

public enum PropertyPreference {

	MUSIC("Music", PropertyConstants.MUSIC_PROPERTIES), BOOK("Book", PropertyConstants.BOOK_PROPERTIES), GAME("Game", PropertyConstants.GAME_PROPERTIES);

	@NonNull
	private final String name;
	@NonNull
	private final Map<String, @NonNull String> propertiesMap;

	private PropertyPreference(@NonNull final String name, @NonNull final Map<String, @NonNull String> propertiesMap) {
		this.name = name;
		this.propertiesMap = propertiesMap;
	}

	@NonNull
	public String getName() {
		return this.name;
	}

	@NonNull
	public Map<String, @NonNull String> getValues() {
		return this.propertiesMap;
	}
}
