package aka.xlstexport.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.eclipse.jdt.annotation.NonNull;

import aka.xlstexport.main.constants.ExportType;

/**
 * Top panel.
 *
 * @author Akazukin
 */
public class TopPanel extends JPanel {

	private static final long serialVersionUID = -4289650088303419650L;
	private final JComboBox<String> comboBox;

	/**
	 * Constructor.
	 */
	public TopPanel() {
		setBackground(Color.WHITE);
		setLayout(new GridBagLayout());
		this.comboBox = new JComboBox<String>();
		this.comboBox.setBackground(Color.WHITE);
		this.comboBox.setPreferredSize(new Dimension(100, 20));
		for (final ExportType exportType : ExportType.values()) {
			this.comboBox.addItem(exportType.getName());
		}
		add(new JLabel("Export Type: "), new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 1, 1));
		add(this.comboBox, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 1, 1));
	}

	/**
	 * Add item listener.
	 *
	 * @param itemListener
	 */
	public void addItemListener(@NonNull final ItemListener itemListener) {
		this.comboBox.addItemListener(itemListener);
	}
}
