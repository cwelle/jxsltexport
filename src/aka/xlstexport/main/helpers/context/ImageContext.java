package aka.xlstexport.main.helpers.context;

public class ImageContext {

	private final boolean createThumbnail;
	private final int thumbnailWidth;
	private final boolean copyImage;
	private final String path;

	public ImageContext(final boolean createThumbnail, final int thumbnailWidth, final boolean copyImage, final String path) {
		this.createThumbnail = createThumbnail;
		this.thumbnailWidth = thumbnailWidth;
		this.copyImage = copyImage;
		this.path = path;
	}

	/**
	 * @return the createThumbnail
	 */
	public boolean isCreateThumbnail() {
		return this.createThumbnail;
	}

	/**
	 * @return the thumbnailWidth
	 */
	public int getThumbnailWidth() {
		return this.thumbnailWidth;
	}

	/**
	 * @return the copyImage
	 */
	public boolean isCopyImage() {
		return this.copyImage;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return this.path;
	}

}
