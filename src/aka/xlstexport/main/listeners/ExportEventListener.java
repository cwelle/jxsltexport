package aka.xlstexport.main.listeners;

import java.util.EventListener;

/**
 * Export Event listener interface.
 *
 * @author Akazukin
 */
public interface ExportEventListener extends EventListener {

	/**
	 * Handle export event.
	 *
	 * @param message
	 */
	void handleExportEvent(String message);

	/**
	 * Increment number of generated files.
	 *
	 * @param numberOfGenerated
	 */
	void addGeneratedFiles(int numberOfGenerated);

	/**
	 * Increment number of copied files.
	 *
	 * @param numberOfCopiedFiles
	 */
	void addCopiedFiles(int numberOfCopiedFiles);

	/**
	 * Increment size copied files.
	 *
	 * @param size
	 */
	void addSizeCopiedFiles(long size);

	/**
	 * Increment number of generated thumbnail.
	 *
	 * @param numberOfThumbnail
	 */
	void addGeneratedThumbnail(int numberOfThumbnail);

	/**
	 * Display summary.
	 */
	void displayConclusion();
}
