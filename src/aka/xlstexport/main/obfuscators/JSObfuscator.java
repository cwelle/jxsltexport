package aka.xlstexport.main.obfuscators;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.eclipse.jdt.annotation.NonNull;
import org.mozilla.javascript.EvaluatorException;

import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

/**
 * JS Obfuscator.
 *
 * @author Akazukin
 */
public class JSObfuscator {

	/**
	 * Obfuscate given file to given output.
	 *
	 * @param inputFile
	 * @param outputFile
	 * @throws IOException
	 */
	public static void obfuscate(@NonNull final File inputFile, @NonNull final String outputFile) throws IOException {
		// first: compress
		InputStreamReader in = new InputStreamReader(new FileInputStream(inputFile), "UTF-8");
		final String localFilename = inputFile.getAbsolutePath();
		assert localFilename != null;
		// Close the input stream first, and then open the output stream,
		// in case the output file should override the input file.
		try {
			final JavaScriptCompressor compressor = new JavaScriptCompressor(in, new JSErrorReporter(localFilename));
			in.close();
			in = null;
			final OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8");
			compressor.compress(out, -1, true, false, true, true);
			out.close();
		} catch (final EvaluatorException e) {
			// error: do not continue, message already displayed
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
		}

		// Now obfuscate
		JPacker.pack(outputFile, outputFile);
	}

	private JSObfuscator() {
		// Singleton
	}
}
