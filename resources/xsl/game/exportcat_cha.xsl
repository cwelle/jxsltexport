<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:php="http://php.net/xsl">
<xsl:include href="../common_cha.xsl"/>
<xsl:output method="html"/>
<!-- the main template -->
<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
<link href="./images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$(document).ready(function(){
		$( "#tabs" ).tabs().addClass('ui-tabs-vertical ui-helper-clearfix'); 
	});
</script>
<link href="csscha.css" rel="stylesheet" media="screen" type="text/css" />
</head>
<body>
    <xsl:apply-templates select="catlist"/>
</body>
</html>
</xsl:template>

<xsl:template match="catlist">
<div id="tabs">
	<ul>
		<xsl:for-each select="cat">
			<xsl:variable name="count" select="position()"/>
		    <li><a href="#tabs-{$count}"><img src="./images/logo_{@url}.png"/>&#160;<xsl:value-of select="@name"/></a></li>
	    </xsl:for-each>
  	</ul>
    <xsl:for-each select="cat">
   		<xsl:variable name="count" select="position()"/>
    	<div id="tabs-{$count}">
		    <iframe style="overflow: hidden;" width="100%" height="100%" name="tabIframeList" src="{@url}/index.html" frameborder="0"></iframe>
    	</div>
	</xsl:for-each>
</div>
</xsl:template> 
</xsl:stylesheet>
