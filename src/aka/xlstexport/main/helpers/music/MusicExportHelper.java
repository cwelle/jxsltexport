package aka.xlstexport.main.helpers.music;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.eclipse.jdt.annotation.NonNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import aka.xlstexport.main.helpers.AbstractExportHelper;
import aka.xlstexport.main.helpers.FilesHelper;
import aka.xlstexport.main.helpers.StringHelper;
import aka.xlstexport.main.helpers.XMLHelper;
import aka.xlstexport.main.helpers.context.FilesContext;
import aka.xlstexport.main.helpers.context.HTMLFileContext;
import aka.xlstexport.main.helpers.context.ImageContext;
import aka.xlstexport.main.helpers.context.MediaContext;
import aka.xlstexport.main.listeners.ExportEventListener;

/**
 * Music export helper.
 *
 * @author Akazukin
 */
public class MusicExportHelper extends AbstractExportHelper {

	private static final Logger log = Logger.getLogger(MusicExportHelper.class.getName());

	private @NonNull final MediaContext mediaContext;

	/**
	 * Constructor.
	 *
	 * @param listenerParem
	 * @param htmlIndexCtxtParam
	 * @param imageContextParam
	 * @param copyFileContextParam
	 * @param mediaContextParam
	 */
	public MusicExportHelper(@NonNull final ExportEventListener listenerParem, @NonNull final HTMLFileContext htmlIndexCtxtParam, @NonNull final ImageContext imageContextParam,
			@NonNull final FilesContext copyFileContextParam, @NonNull final MediaContext mediaContextParam) {
		super(listenerParem, htmlIndexCtxtParam, imageContextParam, copyFileContextParam);
		this.mediaContext = mediaContextParam;
	}

	@Override
	public void execute() {
		if (this.mediaContext.copyMedia()) {
			processCopyMedia();
		}
	}

	private void processCopyMedia() {
		try {
			final File dest = this.htmlCtxt.getDestDir();
			if (dest == null || !dest.exists()) {
				JOptionPane.showMessageDialog(null, "Destination directory does not exist.");
				return;
			}

			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			final Document doc = docBuilder.parse(this.htmlCtxt.getXmlFile());

			final NodeList mainNodesItem = doc.getElementsByTagName(getMainNode());
			for (int m = 0; m < mainNodesItem.getLength(); m++) {
				final Element elMain = (Element) mainNodesItem.item(m);
				final String id = elMain.getElementsByTagName("id").item(0).getTextContent();
				final NodeList mainNodes = elMain.getElementsByTagName("detail");
				for (int i = 0; i < mainNodes.getLength(); i++) {
					final Element el = (Element) mainNodes.item(i);
					final String type = el.getAttribute("type");
					if ("disc".equals(type)) {
						final String cdId = el.getElementsByTagName("index").item(0).getTextContent();
						final NodeList cdNodes = el.getElementsByTagName("detail");
						if (cdNodes.getLength() > 0) {
							// create empty playlist.xml
							final Document playListDoc = XMLHelper.getNewDocument();
							final Element tracklistElement = XMLHelper.getPlaylistPLayListElement(playListDoc);
							final String destDirFilesId = dest.toURI().toURL().getFile() + "files/" + id + "/cd" + cdId + "/";
							final File theDir = new File(destDirFilesId);
							// if the directory does not exist,
							// create it
							if (!theDir.exists()) {
								try {
									theDir.mkdir();
								} catch (final SecurityException se) {
									// handle it
								}
							}
							boolean createPlaylist = false;
							for (int j = 0; j < cdNodes.getLength(); j++) {
								final Node itemCD = cdNodes.item(j);
								if (itemCD.getNodeType() == Node.ELEMENT_NODE) {
									final Element elCD = (Element) cdNodes.item(j);
									final NodeList audioFiles = elCD.getElementsByTagName("audiofile");
									for (int k = 0; k < audioFiles.getLength(); k++) {
										final Element audioFile = (Element) audioFiles.item(k);
										final Element parentNode = (Element) audioFile.getParentNode();
										final String indexTrack = parentNode.getElementsByTagName("index").item(0).getTextContent();

										final Element trackNode = playListDoc.createElement("track");
										tracklistElement.appendChild(trackNode);
										// copy file
										String mediaFile = audioFile.getTextContent();
										mediaFile = StringEscapeUtils.unescapeHtml4(mediaFile);
										assert mediaFile != null;
										mediaFile = StringHelper.decode(mediaFile);
										final String ext = FilenameUtils.getExtension(mediaFile);
										final File file = new File(mediaFile);
										this.listener.addSizeCopiedFiles(FileUtils.sizeOf(file));
										final File destinationFile = new File(destDirFilesId + indexTrack + "." + ext);
										if (this.copyFileContext.isCopyNewDifferentSize()) {
											final boolean mustBeCopied = FilesHelper.isNewOrDifferentSize(destinationFile, file);
											if (mustBeCopied) {
												this.listener.handleExportEvent("Copy " + mediaFile);
												this.listener.addCopiedFiles(1);
												FileUtils.copyFile(file, destinationFile);
												createPlaylist = true;
											}
										} else {
											this.listener.handleExportEvent("Copy " + mediaFile);
											this.listener.addCopiedFiles(1);
											FileUtils.copyFile(file, destinationFile);
											createPlaylist = true;
										}

										final Element locationElement = playListDoc.createElement("location");
										locationElement.setTextContent("./../files/" + id + "/cd" + cdId + "/" + indexTrack + "." + ext);
										trackNode.appendChild(locationElement);
										final NodeList titleNodeList = parentNode.getElementsByTagName("title");
										final Element titleElementTrack = playListDoc.createElement("title");
										if (titleNodeList != null && titleNodeList.getLength() > 0) {
											final String title = parentNode.getElementsByTagName("title").item(0).getTextContent();
											titleElementTrack.setTextContent(title);
										}
										trackNode.appendChild(titleElementTrack);
										XMLHelper.addDefaultElements(playListDoc, trackNode);
									}
								}
							}
							if (theDir.exists() && createPlaylist) {
								try {
									// write the content into xml file
									final TransformerFactory transformerFactory = TransformerFactory.newInstance();
									final Transformer transformer = transformerFactory.newTransformer();
									final DOMSource source = new DOMSource(playListDoc);
									final StreamResult result = new StreamResult(new File(destDirFilesId + "playlist.xml"));
									transformer.transform(source, result);
									this.listener.handleExportEvent("Creating playlist...");
									this.listener.addGeneratedFiles(1);
								} catch (TransformerFactoryConfigurationError | TransformerException e) {
									JOptionPane.showMessageDialog(null, "Can't copy playlist.");
									log.log(Level.SEVERE, e.getMessage());
								}
							}

						}
					}
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}

	@Override
	@NonNull
	public String getMainNode() {
		return "music";
	}

	@Override
	@NonNull
	public String getMainNodeList() {
		return "musiclist";
	}
}
