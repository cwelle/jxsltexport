package aka.xlstexport.main.filters;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.jdt.annotation.NonNull;

/**
 * Class to use in a JFileChooser to select which files can be displayed.
 *
 * @author Akazukin
 */
public class FilesFilter extends FileFilter {

	/**
	 * XML files.
	 */
	@NonNull
	public final static String XML = "xml";
	/**
	 * XSL files.
	 */
	@NonNull
	public final static String XSL = "xsl";
	/**
	 * CSS files.
	 */
	@NonNull
	public final static String CSS = "css";

	private @NonNull final String filterExtension;

	/**
	 * Constructor.
	 *
	 * @param ext
	 *            file extension to use.
	 */
	public FilesFilter(@NonNull final String ext) {
		this.filterExtension = ext;
	}

	@Override
	public boolean accept(final File f) {
		if (f.isDirectory()) {
			return true;
		}

		final String extension = FilenameUtils.getExtension(f.getName());
		if (this.filterExtension.equalsIgnoreCase(extension)) {
			return true;
		}

		return false;
	}

	// The description of this filter
	@Override
	public String getDescription() {
		return this.filterExtension.toUpperCase() + " File(s)";
	}
}
