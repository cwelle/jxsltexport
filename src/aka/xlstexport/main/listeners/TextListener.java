package aka.xlstexport.main.listeners;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Listener to link value to preference item.
 *
 * @author Akazukin
 */
public class TextListener implements DocumentListener {

	private static final Logger log = Logger.getLogger(TextListener.class.getName());

	private final Preferences preferences;
	@NonNull
	private final String preference;

	/**
	 * Constructor.
	 *
	 * @param p
	 *            preference
	 * @param keyPreference
	 *            key
	 */
	public TextListener(@NonNull final Preferences p, @NonNull final String keyPreference) {
		this.preferences = p;
		this.preference = keyPreference;
	}

	@Override
	public void insertUpdate(final DocumentEvent e) {
		updateValue(e);
	}

	@Override
	public void removeUpdate(final DocumentEvent e) {
		updateValue(e);
	}

	@Override
	public void changedUpdate(final DocumentEvent e) {
		updateValue(e);
	}

	private void updateValue(final DocumentEvent e) {
		final Document doc = e.getDocument();
		try {
			this.preferences.put(this.preference, doc.getText(0, doc.getLength()));
		} catch (final BadLocationException e1) {
			log.log(Level.SEVERE, "Unable create pref", e);
		}
	}
}
