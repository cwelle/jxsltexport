package aka.xlstexport.main.runnable;

import javax.swing.JButton;

import org.eclipse.jdt.annotation.NonNull;

import aka.xlstexport.main.helpers.AbstractExportHelper;
import aka.xlstexport.main.helpers.StringHelper;
import aka.xlstexport.main.listeners.ExportEventListener;

/**
 * Export runnable implement runnable to run any AbstractExportHelper in a
 * separate thread.
 *
 * @author Akazukin
 */
public class ExportRunnable implements Runnable {

	@NonNull
	private final JButton exportButton;
	@NonNull
	private final AbstractExportHelper exportHelper;
	@NonNull
	private final ExportEventListener listener;

	/**
	 * Constructor.
	 *
	 * @param exportHelperParam
	 *            Export helper to be run in thread.
	 * @param exportButtonParam
	 *            export button
	 * @param listenerParam
	 *            Export event listener
	 */
	public ExportRunnable(@NonNull final AbstractExportHelper exportHelperParam, @NonNull final JButton exportButtonParam, @NonNull final ExportEventListener listenerParam) {
		this.exportButton = exportButtonParam;
		this.exportHelper = exportHelperParam;
		this.listener = listenerParam;
	}

	@Override
	public void run() {
		final long startTime = System.currentTimeMillis();
		this.exportButton.setEnabled(false);
		this.exportHelper.run();
		this.exportButton.setEnabled(true);
		this.listener.displayConclusion();
		final long stopTime = System.currentTimeMillis();

		this.listener.handleExportEvent("Elapsed Time: " + StringHelper.getReadableElapsedTime(startTime, stopTime));
	}
}
