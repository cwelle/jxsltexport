package aka.xlstexport.main.filters;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.eclipse.jdt.annotation.NonNull;

/**
 * File(s) Filter for FileUtils.listFilesAndDirs method.
 *
 * @author Akazukin
 */
public class IOFilesFilter implements IOFileFilter {

	/**
	 * CSS files.
	 */
	@NonNull
	public final static String CSS = "css";
	/**
	 * JS files.
	 */
	@NonNull
	public final static String JS = "js";
	/**
	 * HTML files.
	 */
	@NonNull
	public final static String HTML = "html";
	@NonNull
	private final String filterExtension;

	/**
	 * Constructor.
	 *
	 * @param fileExt
	 *            file extension to match.
	 */
	public IOFilesFilter(@NonNull final String fileExt) {
		this.filterExtension = fileExt;
	}

	@Override
	public boolean accept(final File f) {
		return acceptFile(f);
	}

	@Override
	public boolean accept(final File f, final String arg1) {
		return acceptFile(f);
	}

	private boolean acceptFile(final File f) {
		if (f.isDirectory()) {
			return false;
		}

		final String extension = FilenameUtils.getExtension(f.getName());
		if (this.filterExtension.equalsIgnoreCase(extension)) {
			return true;
		}
		return false;
	}
}
