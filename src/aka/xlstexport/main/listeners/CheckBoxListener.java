package aka.xlstexport.main.listeners;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.prefs.Preferences;

import javax.swing.JCheckBox;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Listener to link value to preference item.
 *
 * @author Akazukin
 */
public class CheckBoxListener implements ItemListener {

	@NonNull
	private final Preferences preferences;
	@NonNull
	private final String preference;

	/**
	 * Constructor.
	 *
	 * @param p
	 *            preference.
	 * @param keyPreference
	 *            preference key.
	 */
	public CheckBoxListener(@NonNull final Preferences p, @NonNull final String keyPreference) {
		this.preferences = p;
		this.preference = keyPreference;
	}

	@Override
	public void itemStateChanged(final ItemEvent e) {
		final Object component = e.getSource();
		if (component instanceof JCheckBox) {
			final JCheckBox checkBox = (JCheckBox) component;
			this.preferences.putBoolean(this.preference, checkBox.isSelected());
		}
	}
}
