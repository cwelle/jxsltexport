package aka.xlstexport.main.constants;

import org.eclipse.jdt.annotation.NonNull;

import aka.xlstexport.main.config.PropertyPreference;

import com.sun.istack.internal.Nullable;

/**
 * Export types.
 *
 * @author Akazukin
 */
public enum ExportType {

	/**
	 * Music type.
	 */
	MUSIC("music", PropertyPreference.MUSIC),
	/**
	 * Book type.
	 */
	BOOK("book", PropertyPreference.BOOK),
	/**
	 * Game type.
	 */
	GAME("game", PropertyPreference.GAME);

	@NonNull
	private String name;
	@NonNull
	private PropertyPreference preference;

	private ExportType(@NonNull final String nameParam, @NonNull final PropertyPreference preferenceParam) {
		this.name = nameParam;
		this.preference = preferenceParam;
	}

	/**
	 * Get type name.
	 *
	 * @return type name
	 */
	@NonNull
	public String getName() {
		return this.name;
	}

	/**
	 * Get property preference.
	 *
	 * @return property preference
	 */
	@NonNull
	public PropertyPreference getPropertyPreference() {
		return this.preference;
	}

	/**
	 * Get ExportType corresponding of given string.
	 *
	 * @param type
	 * @return ExportType corresponding of given string
	 */
	@Nullable
	public static ExportType getExportType(@Nullable final String type) {
		ExportType result = null;
		if (type != null) {
			for (final ExportType exportType : values()) {
				if (exportType.getName().equals(type)) {
					result = exportType;
					// found, just break
					break;
				}
			}
		}
		return result;
	}
}
