package aka.xlstexport.main.helpers;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * XML Helper.
 *
 * @author Akazukin
 */
public class XMLHelper {

	private static final Logger log = Logger.getLogger(XMLHelper.class.getName());

	/**
	 * Transform given source with given XSL stylesheet.
	 *
	 * @param source
	 * @param stylesheet
	 *            XSL file
	 * @param dest
	 *            destination file
	 * @param params
	 *            transformer params
	 * @throws TransformerException
	 */
	public static void transform(@NonNull final DOMSource source, @NonNull final File stylesheet, @NonNull final String dest, @Nullable final Map<String, String> params) throws TransformerException {
		final TransformerFactory tFactory = TransformerFactory.newInstance();
		final Transformer transformer = tFactory.newTransformer(new StreamSource(stylesheet));
		// preserve unicode
		transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
		if (params != null) {
			for (final Entry<String, String> item : params.entrySet()) {
				transformer.setParameter(item.getKey(), item.getValue());
			}
		}
		transformer.transform(source, new StreamResult(dest));
	}

	/**
	 * Get new document.
	 *
	 * @return new XML Document
	 * @throws ParserConfigurationException
	 */
	@NonNull
	public static Document getNewDocument() throws ParserConfigurationException {
		final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		final Document playListDoc = docBuilder.newDocument();
		playListDoc.setXmlVersion("1.0");
		return playListDoc;
	}

	/**
	 * Get DOMSource of the XML created to represent categories.
	 *
	 * @param platformsMap
	 * @return DOMSource of the categories xml representation.
	 */
	@Nullable
	public static DOMSource getCatDoc(@NonNull final Map<String, String> platformsMap) {
		DOMSource result = null;
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			final Document doc = docBuilder.newDocument();
			doc.setXmlVersion("1.0");
			final Element rootElement = doc.createElement("catlist");
			doc.appendChild(rootElement);

			for (final Entry<String, String> entry : platformsMap.entrySet()) {
				final Element element = doc.createElement("cat");
				element.setAttribute("url", entry.getKey());
				element.setAttribute("name", entry.getValue());
				rootElement.appendChild(element);
			}

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "5");
			result = new DOMSource(rootElement);
		} catch (ParserConfigurationException | TransformerException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		return result;
	}

	/**
	 * Get DOMSource of the XML created to represent navigation.
	 *
	 * @param i
	 *            current page
	 * @param numberOfPages
	 *            total of pages
	 * @return DOMSource of the navigation xml representation.
	 */
	@Nullable
	public static DOMSource getNavDoc(final int i, final double numberOfPages) {
		DOMSource result = null;
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			final Document doc = docBuilder.newDocument();
			doc.setXmlVersion("1.0");
			final Element rootElement = doc.createElement("navigation");
			doc.appendChild(rootElement);

			// first link management
			if (i != 0) {
				final Element node = doc.createElement("firstlink");
				node.setAttribute("url", "index.html");
				rootElement.appendChild(node);
			}
			// last link
			if (i != numberOfPages - 1) {
				final Element node = doc.createElement("lastlink");
				node.setAttribute("url", "index" + (numberOfPages - 1) + ".html");
				rootElement.appendChild(node);
			}
			// first link management
			if (i > 0) {
				final int prevPage = i - 1;
				final Element node = doc.createElement("prevlink");
				if (prevPage == 0) {
					node.setAttribute("url", "index.html");
				} else {
					node.setAttribute("url", "index" + prevPage + ".html");
				}
				rootElement.appendChild(node);
			}
			// last link
			if (i < numberOfPages - 1) {
				final int nextPage = i + 1;
				final Element node = doc.createElement("nextlink");
				node.setAttribute("url", "index" + nextPage + ".html");
				rootElement.appendChild(node);
			}

			for (int j = 0; j < numberOfPages; j++) {
				final Element node = doc.createElement("pagelink");
				if (i != j) {
					if (j == 0) {
						node.setAttribute("url", "index.html");
					} else {
						node.setAttribute("url", "index" + j + ".html");
					}
				}
				node.setAttribute("pagenum", "" + (j + 1));
				rootElement.appendChild(node);
			}

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "5");
			result = new DOMSource(rootElement);
		} catch (ParserConfigurationException | TransformerException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		return result;
	}

	/**
	 * Fill given doc for playlist xml and return tracklist element.
	 *
	 * @param playListDoc
	 * @return rootelement
	 */
	@NonNull
	public static Element getPlaylistPLayListElement(@NonNull final Document playListDoc) {
		final Element rootElement = playListDoc.createElement("playlist");
		playListDoc.appendChild(rootElement);
		final Element titleElement = playListDoc.createElement("title");
		titleElement.setTextContent("Playlist");
		rootElement.appendChild(titleElement);
		final Element creatorElement = playListDoc.createElement("creator");
		creatorElement.setTextContent("Cha");
		rootElement.appendChild(creatorElement);
		final Element linkElement = playListDoc.createElement("link");
		rootElement.appendChild(linkElement);
		final Element infoElement = playListDoc.createElement("info");
		infoElement.setTextContent("Playlist");
		rootElement.appendChild(infoElement);
		final Element imageElement = playListDoc.createElement("image");
		rootElement.appendChild(imageElement);
		final Element tracklistElement = playListDoc.createElement("trackList");
		assert tracklistElement != null;
		rootElement.appendChild(tracklistElement);

		return tracklistElement;
	}

	/**
	 * Add defaults elements to given document in the given element.
	 *
	 * @param playListDoc
	 *            playlist document xml
	 * @param trackNode
	 *            root node to append to
	 */
	public static void addDefaultElements(@NonNull final Document playListDoc, @NonNull final Element trackNode) {
		final Element creatorElementTrack = playListDoc.createElement("creator");
		creatorElementTrack.setTextContent("Cha");
		trackNode.appendChild(creatorElementTrack);
		final Element albumElement = playListDoc.createElement("album");
		trackNode.appendChild(albumElement);
		final Element annotationElement = playListDoc.createElement("annotation");
		trackNode.appendChild(annotationElement);
		final Element durationElement = playListDoc.createElement("duration");
		trackNode.appendChild(durationElement);
		final Element imageElementTrack = playListDoc.createElement("image");
		trackNode.appendChild(imageElementTrack);
		final Element infoElementTrack = playListDoc.createElement("info");
		trackNode.appendChild(infoElementTrack);
		final Element linkElementTrack = playListDoc.createElement("link");
		trackNode.appendChild(linkElementTrack);
	}

	private XMLHelper() {
		// Singleton
	}

}
