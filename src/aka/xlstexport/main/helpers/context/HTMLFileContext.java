package aka.xlstexport.main.helpers.context;

import java.io.File;

import org.eclipse.jdt.annotation.Nullable;

/**
 * HTML file(s) context.
 *
 * @author Akazukin
 */
public class HTMLFileContext {

	@Nullable
	private File xmlFile;
	@Nullable
	private File xslIndexFile;
	@Nullable
	private File destDir;
	@Nullable
	private Integer splitEvery;
	private boolean sortByTitle;
	private boolean compressHTML;
	@Nullable
	private File xslDetailsFile;
	private boolean splitByPlatform;
	private File xslPlatformFile;

	/**
	 * @return the xmlFile
	 */
	@Nullable
	public final File getXmlFile() {
		return this.xmlFile;
	}

	/**
	 * @return the xslIndexFile
	 */
	@Nullable
	public final File getXslIndexFile() {
		return this.xslIndexFile;
	}

	/**
	 * @return the destDir
	 */
	@Nullable
	public final File getDestDir() {
		return this.destDir;
	}

	/**
	 * @return the splitEvery
	 */
	@Nullable
	public final Integer getSplitEvery() {
		return this.splitEvery;
	}

	/**
	 * @return the sortByTitle
	 */
	public final boolean isSortByTitle() {
		return this.sortByTitle;
	}

	/**
	 * @return the compressHTML
	 */
	public final boolean isCompressHTML() {
		return this.compressHTML;
	}

	/**
	 * @return the xslDetailsFile
	 */
	@Nullable
	public final File getXslDetailsFile() {
		return this.xslDetailsFile;
	}

	/**
	 * @param xmlFileParam
	 *            the xmlFile to set
	 */
	public final void setXmlFile(@Nullable final File xmlFileParam) {
		this.xmlFile = xmlFileParam;
	}

	/**
	 * @param xslIndexFileParam
	 *            the xslIndexFile to set
	 */
	public final void setXslIndexFile(@Nullable final File xslIndexFileParam) {
		this.xslIndexFile = xslIndexFileParam;
	}

	/**
	 * @param destDirParam
	 *            the destDir to set
	 */
	public final void setDestDir(@Nullable final File destDirParam) {
		this.destDir = destDirParam;
	}

	/**
	 * @param splitEveryParam
	 *            the splitEvery to set
	 */
	public final void setSplitEvery(@Nullable final Integer splitEveryParam) {
		this.splitEvery = splitEveryParam;
	}

	/**
	 * @param sortByTitleParam
	 *            the sortByTitle to set
	 */
	public final void setSortByTitle(final boolean sortByTitleParam) {
		this.sortByTitle = sortByTitleParam;
	}

	/**
	 * @param compressHTMLParam
	 *            the compressHTML to set
	 */
	public final void setCompressHTML(final boolean compressHTMLParam) {
		this.compressHTML = compressHTMLParam;
	}

	/**
	 * @param xslDetailsFileParam
	 *            the xslDetailsFile to set
	 */
	public final void setXslDetailsFile(@Nullable final File xslDetailsFileParam) {
		this.xslDetailsFile = xslDetailsFileParam;
	}

	/**
	 * @param split
	 */
	public void setSplitByPlatform(final boolean split) {
		this.splitByPlatform = split;
	}

	public boolean isSplitByPlatform() {
		return this.splitByPlatform;
	}

	public void setXslPlatform(final File xslPlatformFileParam) {
		this.xslPlatformFile = xslPlatformFileParam;
	}

	public File getXslPlatformFile() {
		return this.xslPlatformFile;
	}
}
