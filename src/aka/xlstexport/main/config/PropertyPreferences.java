package aka.xlstexport.main.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

public class PropertyPreferences extends AbstractPreferences {

	private static final Logger log = Logger.getLogger(PropertyPreferences.class.getName());

	private final Map<String, String> root;
	private final Map<String, PropertyPreferences> children;
	private boolean isRemoved = false;

	public PropertyPreferences(final AbstractPreferences parent, final String name) {
		super(parent, name);
		log.finest("Instantiating node " + name);

		this.root = new TreeMap<String, String>();
		this.children = new TreeMap<String, PropertyPreferences>();

		try {
			sync();
		} catch (final BackingStoreException e) {
			log.log(Level.SEVERE, "Unable to sync on creation of node " + name, e);
		}
	}

	@Override
	protected void putSpi(final String key, final String value) {
		this.root.put(key, value);
		try {
			flush();
		} catch (final BackingStoreException e) {
			log.log(Level.SEVERE, "Unable to flush after putting " + key, e);
		}
	}

	@Override
	protected String getSpi(final String key) {
		return this.root.get(key);
	}

	@Override
	protected void removeSpi(final String key) {
		this.root.remove(key);
		try {
			flush();
		} catch (final BackingStoreException e) {
			log.log(Level.SEVERE, "Unable to flush after removing " + key, e);
		}
	}

	@Override
	protected void removeNodeSpi() throws BackingStoreException {
		this.isRemoved = true;
		flush();
	}

	@Override
	protected String[] keysSpi() throws BackingStoreException {
		return this.root.keySet().toArray(new String[this.root.keySet().size()]);
	}

	@Override
	protected String[] childrenNamesSpi() throws BackingStoreException {
		return this.children.keySet().toArray(new String[this.children.keySet().size()]);
	}

	@Override
	protected PropertyPreferences childSpi(final String name) {
		PropertyPreferences child = this.children.get(name);
		if (child == null || child.isRemoved()) {
			child = new PropertyPreferences(this, name);
			this.children.put(name, child);
		}
		return child;
	}

	@Override
	protected void syncSpi() throws BackingStoreException {
		if (isRemoved()) {
			return;
		}

		final File file = PropertyPreferencesFactory.getPreferencesFile();
		if (!file.exists()) {
			return;
		}

		synchronized (file) {
			final Properties p = new Properties();
			try {
				p.load(new FileInputStream(file));

				final StringBuilder sb = new StringBuilder();
				getPath(sb);
				final String path = sb.toString();

				final Enumeration<?> pnen = p.propertyNames();
				while (pnen.hasMoreElements()) {
					final String propKey = (String) pnen.nextElement();
					if (propKey.startsWith(path)) {
						final String subKey = propKey.substring(path.length());
						// Only load immediate descendants
						if (subKey.indexOf('.') == -1) {
							this.root.put(subKey, p.getProperty(propKey));
						}
					}
				}
			} catch (final IOException e) {
				throw new BackingStoreException(e);
			}
		}
	}

	private void getPath(final StringBuilder sb) {
		final PropertyPreferences parent = (PropertyPreferences) parent();
		if (parent == null) {
			return;
		}

		parent.getPath(sb);
		sb.append(name()).append('.');
	}

	@Override
	protected void flushSpi() throws BackingStoreException {
		final File file = PropertyPreferencesFactory.getPreferencesFile();

		synchronized (file) {
			final Properties p = new Properties();
			try {

				final StringBuilder sb = new StringBuilder();
				getPath(sb);
				final String path = sb.toString();

				if (file.exists()) {
					p.load(new FileInputStream(file));

					final List<String> toRemove = new ArrayList<String>();

					// Make a list of all direct children of this node to be
					// removed
					final Enumeration<?> pnen = p.propertyNames();
					while (pnen.hasMoreElements()) {
						final String propKey = (String) pnen.nextElement();
						if (propKey.startsWith(path)) {
							final String subKey = propKey.substring(path.length());
							// Only do immediate descendants
							if (subKey.indexOf('.') == -1) {
								toRemove.add(propKey);
							}
						}
					}

					// Remove them now that the enumeration is done with
					for (final String propKey : toRemove) {
						p.remove(propKey);
					}
				}

				// If this node hasn't been removed, add back in any values
				if (!this.isRemoved) {
					for (final String s : this.root.keySet()) {
						p.setProperty(path + s, this.root.get(s));
					}
				}

				p.store(new FileOutputStream(file), "FilePreferences");
			} catch (final IOException e) {
				throw new BackingStoreException(e);
			}
		}
	}
}
