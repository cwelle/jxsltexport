package aka.xlstexport.main.helpers.context;

import java.io.File;

import org.eclipse.jdt.annotation.Nullable;

/**
 * File(s) context.
 *
 * @author Akazukin
 */
public class FilesContext {

	@Nullable
	private File commonDir;
	@Nullable
	private File cssFile;
	private boolean obfuscateCSS;
	private boolean obfuscateJS;
	private boolean copyNewDifferentSize;

	/**
	 * @return the commonDir
	 */
	@Nullable
	public File getCommonDir() {
		return this.commonDir;
	}

	/**
	 * @return the cssFile
	 */
	@Nullable
	public File getCssFile() {
		return this.cssFile;
	}

	/**
	 * @return the obfuscateCSS
	 */
	public boolean isObfuscateCSS() {
		return this.obfuscateCSS;
	}

	/**
	 * @return the obfuscateJS
	 */
	public boolean isObfuscateJS() {
		return this.obfuscateJS;
	}

	/**
	 * @return the copyNewDifferentSize
	 */
	public boolean isCopyNewDifferentSize() {
		return this.copyNewDifferentSize;
	}

	/**
	 * @param commonDirParam
	 *            the commonDir to set
	 */
	public final void setCommonDir(@Nullable final File commonDirParam) {
		this.commonDir = commonDirParam;
	}

	/**
	 * @param cssFileParam
	 *            the cssFile to set
	 */
	public final void setCssFile(@Nullable final File cssFileParam) {
		this.cssFile = cssFileParam;
	}

	/**
	 * @param obfuscateCSSParam
	 *            the obfuscateCSS to set
	 */
	public final void setCompressCSS(final boolean obfuscateCSSParam) {
		this.obfuscateCSS = obfuscateCSSParam;
	}

	/**
	 * @param obfuscateJSParam
	 *            the obfuscateJS to set
	 */
	public final void setObfuscateJS(final boolean obfuscateJSParam) {
		this.obfuscateJS = obfuscateJSParam;
	}

	/**
	 * @param copyNewDifferentSizeParam
	 *            the copyNewDifferentSize to set
	 */
	public final void setCopyNewDifferentSize(final boolean copyNewDifferentSizeParam) {
		this.copyNewDifferentSize = copyNewDifferentSizeParam;
	}
}
