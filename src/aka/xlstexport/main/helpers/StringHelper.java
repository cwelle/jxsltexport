package aka.xlstexport.main.helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * String helper class.
 *
 * @author Akazukin
 */
public class StringHelper {

	private static final Logger log = Logger.getLogger(StringHelper.class.getName());

	final static String[] hex = { "%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07", "%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e", "%0f", "%10", "%11", "%12", "%13", "%14", "%15", "%16",
	        "%17", "%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f", "%20", "%21", "%22", "%23", "%24", "%25", "%26", "%27", "%28", "%29", "%2a", "%2b", "%2c", "%2d", "%2e", "%2f", "%30",
	        "%31", "%32", "%33", "%34", "%35", "%36", "%37", "%38", "%39", "%3a", "%3b", "%3c", "%3d", "%3e", "%3f", "%40", "%41", "%42", "%43", "%44", "%45", "%46", "%47", "%48", "%49", "%4a",
	        "%4b", "%4c", "%4d", "%4e", "%4f", "%50", "%51", "%52", "%53", "%54", "%55", "%56", "%57", "%58", "%59", "%5a", "%5b", "%5c", "%5d", "%5e", "%5f", "%60", "%61", "%62", "%63", "%64",
	        "%65", "%66", "%67", "%68", "%69", "%6a", "%6b", "%6c", "%6d", "%6e", "%6f", "%70", "%71", "%72", "%73", "%74", "%75", "%76", "%77", "%78", "%79", "%7a", "%7b", "%7c", "%7d", "%7e",
	        "%7f", "%80", "%81", "%82", "%83", "%84", "%85", "%86", "%87", "%88", "%89", "%8a", "%8b", "%8c", "%8d", "%8e", "%8f", "%90", "%91", "%92", "%93", "%94", "%95", "%96", "%97", "%98",
	        "%99", "%9a", "%9b", "%9c", "%9d", "%9e", "%9f", "%a0", "%a1", "%a2", "%a3", "%a4", "%a5", "%a6", "%a7", "%a8", "%a9", "%aa", "%ab", "%ac", "%ad", "%ae", "%af", "%b0", "%b1", "%b2",
	        "%b3", "%b4", "%b5", "%b6", "%b7", "%b8", "%b9", "%ba", "%bb", "%bc", "%bd", "%be", "%bf", "%c0", "%c1", "%c2", "%c3", "%c4", "%c5", "%c6", "%c7", "%c8", "%c9", "%ca", "%cb", "%cc",
	        "%cd", "%ce", "%cf", "%d0", "%d1", "%d2", "%d3", "%d4", "%d5", "%d6", "%d7", "%d8", "%d9", "%da", "%db", "%dc", "%dd", "%de", "%df", "%e0", "%e1", "%e2", "%e3", "%e4", "%e5", "%e6",
	        "%e7", "%e8", "%e9", "%ea", "%eb", "%ec", "%ed", "%ee", "%ef", "%f0", "%f1", "%f2", "%f3", "%f4", "%f5", "%f6", "%f7", "%f8", "%f9", "%fa", "%fb", "%fc", "%fd", "%fe", "%ff" };

	/**
	 * Decode the given string if it contains hex character(s).
	 *
	 * @param s
	 *            String to be decoded
	 * @return string utf 8 decoded.
	 */
	@Nullable
	public static String decode(@NonNull final String s) {
		final String result = s;
		for (final String string : hex) {
			if (result.contains(string)) {
				try {
					return URLDecoder.decode(result, "UTF-8");
				} catch (final UnsupportedEncodingException e) {
					log.log(Level.SEVERE, e.getMessage());
				}
			}
		}
		return result;
	}

	/**
	 * Get human readable string for elapsed time between start and stop time.
	 *
	 * @param startTime
	 * @param stopTime
	 * @return a readable (xx min xx sec) string
	 */
	@NonNull
	public static String getReadableElapsedTime(final long startTime, final long stopTime) {
		final long elapsedTime = stopTime - startTime;
		final Long minutes = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(elapsedTime));
		final Long seconds = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(elapsedTime) - TimeUnit.MINUTES.toSeconds(minutes.longValue()));
		final String elapsedTimeString = String.format(" %d min, %d sec", minutes, seconds);
		assert elapsedTimeString != null;
		return elapsedTimeString;
	}

	/**
	 * Get human readable string of given size.
	 *
	 * @param size
	 *            (in byte)
	 * @return readable file size
	 */
	public static String readableFileSize(final long size) {
		if (size <= 0) {
			return "0";
		}
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		final int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}

	/**
	 * Read a file with adding separator.
	 *
	 * @param reader
	 * @return String read from file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Nullable
	public static String buildStringFromTextFile(@NonNull final BufferedReader reader) throws FileNotFoundException, IOException {
		final StringBuilder sb = new StringBuilder();
		String s;
		while ((s = reader.readLine()) != null) {
			sb.append(s).append(System.getProperty("line.separator"));
		}
		reader.close();
		return sb.toString();
	}

	/**
	 * Get page name file of the given index (i).
	 *
	 * @param i
	 *            current page
	 * @return page name
	 */
	@NonNull
	public static String getIndexName(final int i) {
		if (i == 0) {
			return "index.html";
		}
		return "index" + i + ".html";
	}

	private StringHelper() {
		// Singleton
	}
}
