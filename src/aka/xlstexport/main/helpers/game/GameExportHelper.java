package aka.xlstexport.main.helpers.game;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import aka.xlstexport.main.helpers.AbstractExportHelper;
import aka.xlstexport.main.helpers.XMLHelper;
import aka.xlstexport.main.helpers.context.FilesContext;
import aka.xlstexport.main.helpers.context.HTMLFileContext;
import aka.xlstexport.main.helpers.context.ImageContext;
import aka.xlstexport.main.listeners.ExportEventListener;

/**
 * Music export helper.
 *
 * @author Akazukin
 */
public class GameExportHelper extends AbstractExportHelper {

	private static final Logger log = Logger.getLogger(GameExportHelper.class.getName());

	/**
	 * Constructor.
	 *
	 * @param listenerParem
	 * @param htmlCtxtParam
	 * @param imageContextParam
	 * @param copyFileContextParam
	 */
	public GameExportHelper(@NonNull final ExportEventListener listenerParem, @NonNull final HTMLFileContext htmlCtxtParam, @NonNull final ImageContext imageContextParam,
			@NonNull final FilesContext copyFileContextParam) {
		super(listenerParem, htmlCtxtParam, imageContextParam, copyFileContextParam);
	}

	@Override
	public void execute() {
		// if (this.mediaContext.copyMedia()) {
		// processCopyMedia();
		// }
	}

	@Override
	protected void processExportHTML(@NonNull final Document sortedDocument, @NonNull final Document document, @Nullable final String subpath) {
		if (this.htmlCtxt.isSplitByPlatform()) {
			try {
				// Get all platforms available
				final Map<String, String> platformsMap = new HashMap<>();
				final XPath xPath = XPathFactory.newInstance().newXPath();
				final XPathExpression expr = xPath.compile("//platform/displayname");
				final NodeList list = (NodeList) expr.evaluate(sortedDocument, XPathConstants.NODESET);
				for (int i = 0; i < list.getLength(); i++) {
					final Node node = list.item(i);
					final String platform = node.getTextContent();
					final String key = platform.trim().replaceAll(" ", "").toLowerCase();
					if (!platformsMap.containsKey(key)) {
						platformsMap.put(key, platform);
					}
				}
				// For each platform create document
				for (final Entry<String, String> platform : platformsMap.entrySet()) {
					final String path = platform.getKey();
					final String platformName = platform.getValue();
					final Document currentDoc = getEmptyDoc();
					final Node rootNode = currentDoc.getElementsByTagName(getMainNodeList()).item(0);
					if (rootNode != null) {
						final XPathExpression expr2 = xPath.compile("//game");
						final NodeList nodeList = (NodeList) expr2.evaluate(sortedDocument, XPathConstants.NODESET);
						for (int i = 0; i < nodeList.getLength(); i++) {
							// check if same platform
							final XPathExpression expr3 = xPath.compile("./platform/displayname");
							final String currentPlatform = expr3.evaluate(nodeList.item(i));
							if (platformName.equalsIgnoreCase(currentPlatform)) {
								final Node currentNode = nodeList.item(i).cloneNode(true);
								final Node newNode = currentDoc.adoptNode(currentNode);
								rootNode.appendChild(newNode);
							}
						}
					}
					super.processExportHTML(currentDoc, currentDoc, path);
				}
				// process categories index html
				final DOMSource docCat = XMLHelper.getCatDoc(platformsMap);
				final File xslCatFile = this.htmlCtxt.getXslPlatformFile();
				if (docCat != null && xslCatFile != null) {
					final File dir = this.htmlCtxt.getDestDir();
					assert dir != null : "As it has been tested in the beginning of the process, it should not be possible.";
					String dest = dir.toURI().toURL().getFile();
					if (!new File(dest).exists()) {
						new File(dest).mkdir();
					}
					dest = dest + "index.html";
					XMLHelper.transform(docCat, xslCatFile, dest, null);
				}
			} catch (XPathExpressionException | DOMException | TransformerException | MalformedURLException e) {
				log.log(Level.SEVERE, e.getMessage());
			}
		} else {
			super.processExportHTML(sortedDocument, document, null);
		}
	}

	@Override
	@NonNull
	public String getMainNode() {
		return "game";
	}

	@Override
	@NonNull
	public String getMainNodeList() {
		return "gamelist";
	}
}
