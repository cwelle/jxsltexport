package aka.xlstexport.main.obfuscators;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.yahoo.platform.yui.compressor.CssCompressor;

/**
 * CSS Obfuscator.
 *
 * @author Akazukin
 */
public class CSSCompressor {

	/**
	 * Obfuscate given file to given output.
	 *
	 * @param inputFile
	 * @param outputFile
	 * @throws IOException
	 */
	public static void obfuscate(final File inputFile, final String outputFile) throws IOException {
		InputStreamReader in = new InputStreamReader(new FileInputStream(inputFile), "UTF-8");
		final CssCompressor compressor = new CssCompressor(in);

		in.close();
		in = null;

		final OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8");

		compressor.compress(out, -1);
		out.close();
	}

	private CSSCompressor() {
		// Singleton
	}
}
