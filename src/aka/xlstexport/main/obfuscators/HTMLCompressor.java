package aka.xlstexport.main.obfuscators;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import aka.xlstexport.main.helpers.FilesHelper;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import com.googlecode.htmlcompressor.compressor.HtmlCompressorStatistics;

/**
 * Compress HTML File(s).
 *
 * @author Akazukin
 */
public class HTMLCompressor {

	private static final Logger log = Logger.getLogger(HTMLCompressor.class.getName());

	/**
	 * Compress the given HTML file.
	 *
	 * @param htmlFile
	 * @return statistics of the compression.
	 */
	@Nullable
	public static HtmlCompressorStatistics compressHTML(@NonNull final File htmlFile) {
		try {
			final String html = FilesHelper.readFile(htmlFile);
			final HtmlCompressor compressor = new HtmlCompressor();
			compressor.setGenerateStatistics(true);
			compressor.setRemoveQuotes(true);
			compressor.setSimpleDoctype(true);
			compressor.setRemoveScriptAttributes(true);
			compressor.setRemoveStyleAttributes(true);
			compressor.setRemoveLinkAttributes(true);
			compressor.setRemoveFormAttributes(true);
			compressor.setRemoveInputAttributes(true);
			compressor.setSimpleBooleanAttributes(true);
			compressor.setRemoveJavaScriptProtocol(true);
			compressor.setRemoveHttpProtocol(false);
			compressor.setRemoveHttpsProtocol(false);
			compressor.setPreserveLineBreaks(false);
			compressor.setRemoveSurroundingSpaces("br,p");
			compressor.setCompressCss(true);
			compressor.setCompressJavaScript(true);
			compressor.setYuiCssLineBreak(80);
			compressor.setYuiJsDisableOptimizations(true);
			compressor.setYuiJsLineBreak(-1);
			compressor.setYuiJsNoMunge(false);
			compressor.setYuiJsPreserveAllSemiColons(false);

			final String compressedHtml = compressor.compress(html);
			final PrintWriter out = new PrintWriter(htmlFile);
			out.println(compressedHtml);
			out.close();
			return compressor.getStatistics();
		} catch (final IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
		return null;
	}

	private HTMLCompressor() {
		// Singleton
	}
}
