package aka.xlstexport.main.obfuscators;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.eclipse.jdt.annotation.NonNull;

import aka.xlstexport.main.helpers.StringHelper;

import com.jpacker.JPackerEncoding;
import com.jpacker.JPackerExecuter;

/**
 * Class using JPacker (https://code.google.com/p/jpacker/).
 *
 * @author Akazukin
 */
public class JPacker {

	/**
	 * Pack the given javascript file with normal encoding.
	 *
	 * @param inputFilename
	 *            javascript file
	 * @param outputFilename
	 *            destination file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void pack(@NonNull final String inputFilename, @NonNull final String outputFilename) throws FileNotFoundException, IOException {
		final JPackerExecuter executer = new JPackerExecuter(JPackerEncoding.NORMAL);
		final BufferedReader in = new BufferedReader(new FileReader(new File(inputFilename)));
		final String unpacked = StringHelper.buildStringFromTextFile(in);
		if (unpacked != null && !unpacked.isEmpty()) {
			final String packed = executer.pack(unpacked, false, true);
			final Writer out = new OutputStreamWriter(new FileOutputStream(outputFilename));
			final String output = packed.replace("\n", System.getProperty("line.separator"));
			in.close();
			out.write(output);
			out.close();
		} else {
			in.close();
		}
	}
}
