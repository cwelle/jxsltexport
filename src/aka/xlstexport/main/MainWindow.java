package aka.xlstexport.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Main window.
 *
 * @author Akazukin
 */
public class MainWindow extends JFrame implements ActionListener {

	private static final long serialVersionUID = 8129290766286698783L;
	private TopPanel topPanel;
	private DetailsPanel detailsPanel;
	private BottomPanel bottomPanel;
	@NonNull
	private final Preferences preferences;
	private JMenuItem helpItem;
	private JMenuItem aboutItem;

	/**
	 * Constructor.
	 *
	 * @param p
	 *            storing preferences.
	 */
	public MainWindow(@NonNull final Preferences p) {
		this.preferences = p;
		initWindow();
		addMenus();
		addPanels();
	}

	private void addMenus() {
		final JMenuBar jMenuBar = new JMenuBar();

		final JMenu menu = new JMenu("Help");

		// Help
		this.helpItem = new JMenuItem("Help");
		this.helpItem.addActionListener(this);
		menu.add(this.helpItem);

		// About
		this.aboutItem = new JMenuItem("About");
		this.aboutItem.addActionListener(this);
		menu.add(this.aboutItem);

		jMenuBar.add(menu);

		setJMenuBar(jMenuBar);
	}

	private void addPanels() {
		final JPanel globalContainer = new JPanel();
		globalContainer.setLayout(new GridBagLayout());
		globalContainer.setBackground(Color.WHITE);
		setContentPane(globalContainer);
		this.topPanel = new TopPanel();
		globalContainer.add(this.topPanel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 1, 1));
		final DetailsPanel currentDetailsPanel = new DetailsPanel(this.preferences);
		this.detailsPanel = currentDetailsPanel;
		this.topPanel.addItemListener(currentDetailsPanel);
		globalContainer.add(this.detailsPanel, new GridBagConstraints(0, 1, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 1, 1));
		final BottomPanel currentBottomPanel = new BottomPanel();
		this.bottomPanel = currentBottomPanel;
		this.detailsPanel.addMessageHandler(currentBottomPanel);
		globalContainer.add(this.bottomPanel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 1, 1));
	}

	private void initWindow() {
		setTitle("XSLTExport");
		setSize(1024, 900);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		final Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
		final Point newLocation = new Point(middle.x - (getWidth() / 2), middle.y - (getHeight() / 2));
		setLocation(newLocation);
		setResizable(false);
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final Object component = e.getSource();
		if (component == this.aboutItem) {
			displayAbout();
		} else if (component == this.helpItem) {
			//
		}
	}

	private static void displayAbout() {
		final String aboutMessage = "XSLTExport\n\nCreated by Welle Charlotte.";
		JOptionPane.showMessageDialog(null, aboutMessage, "About", JOptionPane.INFORMATION_MESSAGE);
	}
}
