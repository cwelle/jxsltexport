package aka.xlstexport.main.helpers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import aka.xlstexport.main.filters.IOFilesFilter;
import aka.xlstexport.main.helpers.context.FilesContext;
import aka.xlstexport.main.helpers.context.HTMLFileContext;
import aka.xlstexport.main.helpers.context.ImageContext;
import aka.xlstexport.main.listeners.ExportEventListener;
import aka.xlstexport.main.obfuscators.CSSCompressor;
import aka.xlstexport.main.obfuscators.HTMLCompressor;
import aka.xlstexport.main.obfuscators.JSObfuscator;

import com.googlecode.htmlcompressor.compressor.HtmlCompressorStatistics;

/**
 * Abstract export helper. Must be extends to be used.
 *
 * @author Akazukin
 */
public abstract class AbstractExportHelper {

	private static final Logger log = Logger.getLogger(AbstractExportHelper.class.getName());

	@NonNull
	private final ImageContext imageContext;
	@NonNull
	protected final HTMLFileContext htmlCtxt;
	@NonNull
	protected final ExportEventListener listener;
	@NonNull
	protected final FilesContext copyFileContext;

	/**
	 * Constructor.
	 *
	 * @param listenerParam
	 * @param htmlIndexCtxtParam
	 * @param imageContextParam
	 * @param copyFileContextParam
	 */
	public AbstractExportHelper(@NonNull final ExportEventListener listenerParam, @NonNull final HTMLFileContext htmlIndexCtxtParam, @NonNull final ImageContext imageContextParam,
			@NonNull final FilesContext copyFileContextParam) {
		this.htmlCtxt = htmlIndexCtxtParam;
		this.imageContext = imageContextParam;
		this.listener = listenerParam;
		this.copyFileContext = copyFileContextParam;
	}

	/**
	 * Run export.
	 */
	public void run() {
		// check sub dir existence
		try {
			final File dest = this.htmlCtxt.getDestDir();
			if (dest == null) {
				JOptionPane.showMessageDialog(null, "Destination directory does not exist.");
				return;
			}
			if (!dest.exists()) {
				dest.mkdir();
			}
			final String destDir = dest.toURI().toURL().getFile();
			final String imageDir = destDir + "images/";
			File dir = new File(imageDir);
			if (!dir.exists()) {
				dir.mkdir();
			}
			final String filesDir = destDir + "files/";
			dir = new File(filesDir);
			if (!dir.exists()) {
				dir.mkdir();
			}
			final String detailsDir = destDir + "details/";
			dir = new File(detailsDir);
			if (!dir.exists()) {
				dir.mkdir();
			}
		} catch (final MalformedURLException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		this.listener.handleExportEvent("Beginning...");

		try {
			// export index html and details
			final Document sortedDocument = getDocument();
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			final Document document = docBuilder.parse(this.htmlCtxt.getXmlFile());
			assert document != null;
			processExportHTML(sortedDocument, document, null);
		} catch (final ParserConfigurationException | SAXException | IOException | TransformerException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		// copy images
		if (this.imageContext.isCopyImage()) {
			this.listener.handleExportEvent("Beginning copy images...");
			processCopyImages();
			this.listener.handleExportEvent("Copy images: Done");
			this.listener.handleExportEvent("Beginning copy screenshots...");
			processCopyScreenshots();
			this.listener.handleExportEvent("Copy screenshots: Done");
		}
		if (this.imageContext.isCreateThumbnail()) {
			this.listener.handleExportEvent("Beginning creating thumbnail...");
			processCreateThumbnail();
			this.listener.handleExportEvent("Copy creating thumbnail: Done");
		}

		// Copy files
		processCopyFiles();

		try {
			processCompressAndObfuscateFiles();
		} catch (final IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		execute();
		this.listener.handleExportEvent("ALL DONE!");
	}

	private void processExportDetailsHTML(@NonNull final Document document, @Nullable final Map<String, String> detailsIdIndexIdMap, @Nullable final Map<String, String> params,
			@Nullable final String category) {
		try {
			final File stylesheet = this.htmlCtxt.getXslDetailsFile();
			if (stylesheet == null) {
				return;
			}
			final File dir = this.htmlCtxt.getDestDir();
			assert dir != null : "As it has been tested in the beginning of the process, it should not be possible.";

			final NodeList mainNodes = document.getElementsByTagName(getMainNode());
			final XPath xPath = XPathFactory.newInstance().newXPath();
			for (int i = 0; i < mainNodes.getLength(); i++) {
				final Element item = (Element) mainNodes.item(i);
				final String id = xPath.compile("./id").evaluate(item);
				String indexName = StringHelper.getIndexName(0);
				if (detailsIdIndexIdMap != null) {
					indexName = detailsIdIndexIdMap.get(id);
				}
				this.listener.handleExportEvent("Processing '" + id + ".html'");
				this.listener.addGeneratedFiles(1);
				final DOMSource source = new DOMSource(item);
				String destDir = dir.toURI().toURL().getFile();
				if (category != null) {
					destDir = destDir + category + "/";
				}
				destDir = destDir + "details/";
				final File destDirFile = new File(destDir);
				if (!destDirFile.exists()) {
					destDirFile.mkdir();
				}
				final String dest = destDir + id + ".html";
				final Map<String, String> parameters = new HashMap<>();
				parameters.putAll(params);
				parameters.put("IndexFileName", indexName);
				XMLHelper.transform(source, stylesheet, dest, parameters);
			}
		} catch (IOException | TransformerException | XPathExpressionException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}

	private void processCopyFiles() {
		try {
			processCSSFile();
			processFiles();
		} catch (final IOException e) {
			JOptionPane.showMessageDialog(null, "File not found or wrong encoding.");
		}
	}

	private void processFiles() throws IOException {
		this.listener.handleExportEvent("Copy File(s) & (Sub)Directory(ies)...");
		final List<File> files = (List<File>) FileUtils.listFiles(this.copyFileContext.getCommonDir(), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		this.listener.addCopiedFiles(files.size());
		FileUtils.copyDirectory(this.copyFileContext.getCommonDir(), this.htmlCtxt.getDestDir());
	}

	private void processCompressAndObfuscateFiles() throws IOException {
		if (this.copyFileContext.isObfuscateCSS()) {
			// List files and folders in that directory
			final IOFilesFilter filesFilter = new IOFilesFilter(IOFilesFilter.CSS);
			final Collection<File> listOfFiles = FileUtils.listFilesAndDirs(this.htmlCtxt.getDestDir(), filesFilter, TrueFileFilter.TRUE);

			for (final File file : listOfFiles) {
				if (!file.isDirectory()) {
					this.listener.handleExportEvent("Compressing " + file.getName());
					CSSCompressor.obfuscate(file, file.getAbsolutePath());
				}
			}
		}
		if (this.copyFileContext.isObfuscateJS()) {
			// List files and folders in that directory
			final IOFilesFilter filesFilter = new IOFilesFilter(IOFilesFilter.JS);
			final Collection<File> listOfFiles = FileUtils.listFilesAndDirs(this.htmlCtxt.getDestDir(), filesFilter, TrueFileFilter.TRUE);

			for (final File file : listOfFiles) {
				final String filePath = file.getAbsolutePath();
				if (!file.isDirectory() && filePath != null) {
					JSObfuscator.obfuscate(file, filePath);
					this.listener.handleExportEvent("Obfuscating " + file.getName());
				}
			}
		}
		if (this.htmlCtxt.isCompressHTML()) {
			// List files and folders in that directory
			final IOFilesFilter filesFilter = new IOFilesFilter(IOFilesFilter.HTML);
			final Collection<File> listOfFiles = FileUtils.listFilesAndDirs(this.htmlCtxt.getDestDir(), filesFilter, TrueFileFilter.TRUE);

			for (final File file : listOfFiles) {
				final String filePath = file.getAbsolutePath();
				if (!file.isDirectory() && filePath != null) {
					this.listener.handleExportEvent("Compressing " + file.getName());
					final HtmlCompressorStatistics statistics = HTMLCompressor.compressHTML(file);
				}
			}
		}
	}

	private void processCSSFile() throws IOException {
		final File cssFile = this.copyFileContext.getCssFile();
		if (cssFile != null) {
			// Copy css into root
			final File dir = this.htmlCtxt.getDestDir();
			assert dir != null : "As it has been tested in the beginning of the process, it should not be possible.";
			final Path dest = Paths.get(dir.toString(), cssFile.getName());
			this.listener.handleExportEvent("Copy '" + cssFile.getName() + "'");
			this.listener.addCopiedFiles(1);
			Files.copy(cssFile.toPath(), dest, StandardCopyOption.REPLACE_EXISTING);
		}
	}

	protected void processExportHTML(@NonNull final Document sortedDocument, @NonNull final Document document, @Nullable final String subpath) {
		Map<String, String> mapId = null;
		try {
			final File stylesheet = this.htmlCtxt.getXslIndexFile();
			assert stylesheet != null;
			final File dir = this.htmlCtxt.getDestDir();
			assert dir != null : "As it has been tested in the beginning of the process, it should not be possible.";

			final Node rootNode = sortedDocument.getFirstChild();

			final int numberOfItems = getNumberOfMainNode();
			final Integer splitEvery = this.htmlCtxt.getSplitEvery();
			if (splitEvery == null || splitEvery.intValue() <= 0 || numberOfItems <= splitEvery.intValue()) {
				final String indexFileName = StringHelper.getIndexName(0);
				this.listener.handleExportEvent("Processing '" + indexFileName + "'");
				this.listener.addGeneratedFiles(1);
				final DOMSource source = new DOMSource(rootNode);
				String dest = dir.toURI().toURL().getFile();
				if (subpath != null) {
					dest = dest + subpath + "/";
				}
				if (!new File(dest).exists()) {
					new File(dest).mkdir();
				}
				dest = dest + StringHelper.getIndexName(0);
				XMLHelper.transform(source, stylesheet, dest, null);
			} else {
				// Split document
				mapId = new HashMap<>();
				final int numberOfItemsByPage = splitEvery.intValue();
				final NodeList mainNodes = sortedDocument.getElementsByTagName(getMainNode());
				final float numberOfPagesFloat = Double.valueOf(numberOfItems).floatValue() / Double.valueOf(numberOfItemsByPage).floatValue();
				final double numberOfPages = Math.ceil(numberOfPagesFloat);
				for (int i = 0; i < numberOfPages; i++) {
					final String indexName = StringHelper.getIndexName(i);
					this.listener.handleExportEvent("Processing '" + indexName + "'");
					this.listener.addGeneratedFiles(1);
					final DOMSource source = getSubDocument(mainNodes, i, numberOfItemsByPage, numberOfItems, numberOfPages, mapId);
					if (source != null) {
						String dest = dir.toURI().toURL().getFile();
						if (subpath != null) {
							dest = dest + subpath + "/";
						}
						if (!new File(dest).exists()) {
							new File(dest).mkdir();
						}
						dest = dest + indexName;
						XMLHelper.transform(source, stylesheet, dest, null);
					}
				}
			}
		} catch (IOException | TransformerException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		// export details html
		final Map<String, String> mapParams = new HashMap<>();
		if (subpath != null) {
			mapParams.put("Category", subpath);
		}
		processExportDetailsHTML(document, mapId, mapParams, subpath);
	}

	@NonNull
	protected Document getDocument() throws ParserConfigurationException, SAXException, IOException, TransformerException {
		final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document document = docBuilder.parse(this.htmlCtxt.getXmlFile());
		assert document != null;
		if (this.htmlCtxt.isSortByTitle()) {
			final TransformerFactory tFactory = TransformerFactory.newInstance();
			final InputStream stream = getClass().getResourceAsStream("/sortbytitle.xsl");
			if (stream == null) {
				JOptionPane.showMessageDialog(null, "Resource not located.");
			} else {
				final Transformer transformer = tFactory.newTransformer(new StreamSource(stream));
				final StringWriter tampon = new StringWriter();
				final PrintWriter fluxSortie = new PrintWriter(tampon);
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
				transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
				transformer.transform(new DOMSource(document.getFirstChild()), new StreamResult(fluxSortie));
				final InputSource is = new InputSource(new StringReader(tampon.toString()));
				document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
				assert document != null;
			}
		}

		return document;
	}

	@Nullable
	protected DOMSource getSubDocument(final NodeList mainNodes, final int i, final int numberOfItemsByPage, final int numberOfItems, final double numberOfPages, final Map<String, String> mapId) {
		int nbToCopy = numberOfItemsByPage * (i + 1);
		if (nbToCopy > numberOfItems) {
			nbToCopy = numberOfItems;
		}
		final Document emptyDoc = getEmptyDoc();
		final Node rootNode = emptyDoc.getElementsByTagName(getMainNodeList()).item(0);
		if (rootNode != null) {
			for (int j = (numberOfItemsByPage * i); j < nbToCopy; j++) {
				final Node currentNode = mainNodes.item(j).cloneNode(true);
				final Node newNode = emptyDoc.adoptNode(currentNode);
				final String id = ((Element) newNode).getElementsByTagName("id").item(0).getTextContent();
				final String indexFileName = StringHelper.getIndexName(i);
				mapId.put(id, indexFileName);
				rootNode.appendChild(newNode);
			}
			final DOMSource docNav = XMLHelper.getNavDoc(i, numberOfPages);
			if (docNav != null) {
				final Node newChild = emptyDoc.adoptNode(docNav.getNode());
				emptyDoc.getFirstChild().appendChild(newChild);
			}
			return new DOMSource(emptyDoc.getFirstChild());
		}
		return null;
	}

	protected int getNumberOfMainNode() {
		int result = 0;
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			final Document document = docBuilder.parse(this.htmlCtxt.getXmlFile());

			final NodeList mainNodes = document.getElementsByTagName(getMainNode());
			result = mainNodes.getLength();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		return result;
	}

	protected Document getEmptyDoc() {
		Document result = null;
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			result = docBuilder.parse(this.htmlCtxt.getXmlFile());

			final NodeList mainNodes = result.getElementsByTagName(getMainNode());
			while (mainNodes.getLength() > 0) {
				final Node node = mainNodes.item(0);
				node.getParentNode().removeChild(node);
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}

		return result;
	}

	/**
	 * Get main node name.
	 *
	 * @return main node name
	 */
	public abstract String getMainNode();

	/**
	 * Get name of the main node list.
	 *
	 * @return string of the main node list
	 */
	public abstract String getMainNodeList();

	/**
	 * Execute specific export.
	 */
	public abstract void execute();

	private void processCopyImages() {
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			final Document doc = docBuilder.parse(this.htmlCtxt.getXmlFile());

			final File dir = this.htmlCtxt.getDestDir();
			assert dir != null : "As it has been tested in the beginning of the process, it should not be possible.";
			final String dest = dir.toURI().toURL().getFile() + "images/";
			final NodeList mainNodes = doc.getElementsByTagName(getMainNode());
			for (int i = 0; i < mainNodes.getLength(); i++) {
				final Node item = mainNodes.item(i);
				if (item.getNodeType() == Node.ELEMENT_NODE) {
					final Element el = (Element) mainNodes.item(i);
					final String id = el.getElementsByTagName("id").item(0).getTextContent();
					final NodeList coverFronts = el.getElementsByTagName("coverfront");
					if (coverFronts.getLength() > 0) {
						final Node coverFront = coverFronts.item(0);
						final String img = coverFront.getTextContent();
						// copy image
						this.listener.handleExportEvent("Copy image '" + img + "'");
						this.listener.addCopiedFiles(1);
						FileUtils.copyFile(new File(img), new File(dest + id + "f.jpg"));
					}
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}

	private void processCopyScreenshots() {
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			final Document doc = docBuilder.parse(this.htmlCtxt.getXmlFile());

			final File dir = this.htmlCtxt.getDestDir();
			assert dir != null : "As it has been tested in the beginning of the process, it should not be possible.";
			final String dest = dir.toURI().toURL().getFile() + "images/";
			final XPath xPath = XPathFactory.newInstance().newXPath();
			final XPathExpression expr = xPath.compile("//links/link[urltype = 'Image']");
			final NodeList list = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < list.getLength(); i++) {
				final Element node = (Element) list.item(i);
				final String type = node.getElementsByTagName("urltype").item(0).getTextContent();
				if ("Image".equals(type)) {
					final String img = node.getElementsByTagName("url").item(0).getTextContent();
					// copy image
					this.listener.handleExportEvent("Copy screenshot '" + img + "'");
					this.listener.addCopiedFiles(1);
					FileUtils.copyFile(new File(img), new File(dest + new File(img).getName()));
				}
			}
		} catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}

	private void processCreateThumbnail() {
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			final Document doc = docBuilder.parse(this.htmlCtxt.getXmlFile());
			final File dir = this.htmlCtxt.getDestDir();
			assert dir != null : "As it has been tested in the beginning of the process, it should not be possible.";

			final String dest = dir.toURI().toURL().getFile() + "images/";
			final NodeList mainNodes = doc.getElementsByTagName(getMainNode());
			for (int i = 0; i < mainNodes.getLength(); i++) {
				final Node item = mainNodes.item(i);
				if (item.getNodeType() == Node.ELEMENT_NODE) {
					final Element el = (Element) mainNodes.item(i);
					final String id = el.getElementsByTagName("id").item(0).getTextContent();
					final NodeList coverFronts = el.getElementsByTagName("coverfront");
					if (coverFronts.getLength() > 0) {
						final Node coverFront = coverFronts.item(0);
						final String img = coverFront.getTextContent();
						// copy image
						createThumb(img, dest, id, this.imageContext.getThumbnailWidth(), "t");
					}
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}

	private void createThumb(final String img, final String dest, final String id, final int newWidth, final String suffix) {
		try {
			final BufferedImage bimg = ImageIO.read(new File(img));
			final int width = bimg.getWidth();
			final int height = bimg.getHeight();

			final float newHeightFloat = Double.valueOf(height).floatValue() * (Double.valueOf(newWidth).floatValue() / Double.valueOf(width).floatValue());
			final double newHeight = Math.floor(newHeightFloat);
			this.listener.handleExportEvent("Creating thumbnail of '" + img + "'");
			this.listener.addGeneratedThumbnail(1);
			Thumbnails.of(img).size(newWidth, Double.valueOf(newHeight).intValue()).toFile(dest + id + "t.jpg");
		} catch (final IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}
}
