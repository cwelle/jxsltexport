package aka.xlstexport.main;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultCaret;

import aka.xlstexport.main.helpers.StringHelper;
import aka.xlstexport.main.listeners.ExportEventListener;

/**
 * Bottom panel.
 *
 * @author Akazukin
 */
public class BottomPanel extends JPanel implements ExportEventListener {

	private static final long serialVersionUID = -1519000938576535880L;
	private final JTextArea textArea;
	private int nbOfGeneratedFiles;
	private int nbOfCopiedFiles;
	private long kb;
	private int nbOfThumbnail;

	/**
	 * Constructor.
	 */
	public BottomPanel() {
		setLayout(new GridBagLayout());
		setBackground(Color.WHITE);
		this.textArea = new JTextArea("");
		this.textArea.setBackground(Color.BLACK);
		this.textArea.setForeground(Color.WHITE);
		this.textArea.setEditable(false);
		this.textArea.setLineWrap(true);
		this.textArea.setWrapStyleWord(true);
		final DefaultCaret caret = (DefaultCaret) this.textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		final JScrollPane zoneScrolable = new JScrollPane(this.textArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(zoneScrolable, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 1, 1));
	}

	@Override
	public void handleExportEvent(final String message) {
		this.textArea.append(message + "\n");
	}

	@Override
	public void addGeneratedFiles(final int numberOfGenerated) {
		this.nbOfGeneratedFiles = this.nbOfGeneratedFiles + numberOfGenerated;
	}

	@Override
	public void addCopiedFiles(final int numberOfCopiedFiles) {
		this.nbOfCopiedFiles = this.nbOfCopiedFiles + numberOfCopiedFiles;
	}

	@Override
	public void addSizeCopiedFiles(final long kbytes) {
		this.kb = this.kb + kbytes;
	}

	@Override
	public void addGeneratedThumbnail(final int numberOfThumbnail) {
		this.nbOfThumbnail = this.nbOfThumbnail + numberOfThumbnail;
	}

	@Override
	public void displayConclusion() {
		this.textArea.append("----------------------------------------------------------------------------------------\n");
		this.textArea.append("----------------------------------------------------------------------------------------\n");
		this.textArea.append("Summary\n");
		this.textArea.append("---------------\n");
		this.textArea.append("\n");
		this.textArea.append(this.nbOfGeneratedFiles + " file(s) generated.\n");
		this.textArea.append(this.nbOfCopiedFiles + " file(s) copied.\n");
		this.textArea.append(this.nbOfThumbnail + " thumbnail(s) generated.\n");
		this.textArea.append(StringHelper.readableFileSize(this.kb) + " of media files.\n");
	}
}
