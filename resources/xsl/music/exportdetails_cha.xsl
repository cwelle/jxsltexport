﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="../common_cha.xsl"/>
<xsl:output method="html"/>
<xsl:param name="IndexFileName" />
<xsl:template match="music">
<html>
<head>
<title><xsl:value-of select="title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
<link href="../images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$(document).ready(function(){
		$( "#tabs" ).tabs(); 
	});
</script>
<script src="../script/script.js" type="text/javascript"></script>
<link href="./../csscha.css" rel="stylesheet" media="screen" type="text/css" />
<link href="./../cssmusic.css" rel="stylesheet" media="screen" type="text/css" />
</head>
<body class="music app music page-detail">
<div class="wrap">
	<a href="./../{$IndexFileName}" class="toolbar-button appbar-collection-button"><i class="fa fa-chevron-left fa-lg"></i> <span class="toolbar-button-text">&lt; Back</span></a>
     <div class="container content ">
        <div class="container">
			<div id="content-data">
    			<div class="static-content">
			        <div class="item-title top-title">
			     	 	<h1>
			     	 		<xsl:for-each select="artists/artist">
					      		<xsl:if test="position()=1"><xsl:value-of select="displayname" /> - <xsl:value-of select="title"/></xsl:if>
					    	</xsl:for-each>
					    	<xsl:value-of select="title"/>
			         	</h1>
				    </div>
				    <!-- COVER -->
				    <div class="item-image">
				    	<xsl:choose>
							<xsl:when test="thumbfilepath!=''">
								<img src="../images/{id}f.jpg" class="item-frontcover lightbox"/>
							</xsl:when>
							<xsl:otherwise>
								<img src="../images/mainitem.jpg" class="item-frontcover lightbox"/>
							</xsl:otherwise>
						</xsl:choose>
				    </div>
			    	<!-- END COVER -->
			        <div id="lightbox-container">
			     	 	<img id="lightbox-img"/>
			    	</div>
			    	<!-- EXTRA DETAILS -->
				    <div class="item-detail">
						<div class="item-genre lightColor">
						   	<xsl:for-each select="genres/genre">
						   		<xsl:if test="position() > 1"> / </xsl:if>
						   		<xsl:value-of select="displayname" /> 
						 	</xsl:for-each>
						</div>
						<div class="item-details2 lightColor"><xsl:if test="label/displayname!=''"><xsl:value-of select="label/displayname"/></xsl:if>
						<xsl:if test="releasedate/date!=''"> (<xsl:call-template name="formatDate2"><xsl:with-param name="value" select="releasedate/date"/></xsl:call-template>)</xsl:if></div>
						<div class="item-details3 lightColor">
							<b><xsl:value-of select="format/displayname" /></b> 
							- <xsl:if test="nrdiscs!=''"><span class="item-discs"><xsl:value-of select="nrdiscs"/> Disque<xsl:if test="nrdiscs != '1'">s</xsl:if></span></xsl:if>
							- <xsl:if test="nrtracks!=''"><span class="item-tracks"><xsl:value-of select="nrtracks"/> Piste<xsl:if test="nrtracks != '1'">s</xsl:if></span></xsl:if>
							<xsl:if test="length!='00:00'"><span class="item-length"> (<xsl:value-of select="length"/>)</span></xsl:if>
			   			</div>
						<div class="item-editioninfo"><xsl:value-of select="labelnumber"/></div>
					</div>
			  		<!-- END EXTRA DETAILS -->
					<!-- PERSONAL -->
					<div class="item-personalbox">
			        	<div class="item-myrating">
			        		<xsl:call-template name="star">
			        			<xsl:with-param name="num" select="rating"/>
			        			<xsl:with-param name="relativePath" select="'./../'"/>
			        		</xsl:call-template>
			       		</div>
				        <table>
				            <tr class="personal"><th>Quantité</th><td> <span><xsl:value-of select="quantity"/></span></td></tr>
				            <tr class="personal"><th>Date d'ajout</th><td> <span><xsl:call-template name="formatDate"><xsl:with-param name="value" select="dateadded/date"/></xsl:call-template></span></td></tr>
				        </table>
			   		</div>
			     	<!-- END PERSONAL -->
			     	<xsl:choose>
			          <xsl:when test="nrdiscs!=''">
			          		<div class="clearfix"></div>
   							<hr class="seperator"/>
					  		<xsl:apply-templates select="details">
					  			<xsl:with-param name="value" select="id"/>
					  		</xsl:apply-templates>
			          </xsl:when>
			        </xsl:choose> 
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
</xsl:template>

<xsl:template match="details">
	<xsl:param name="value"/>
	<div id="tabs">
	  <ul>
	  	  <xsl:for-each select="detail[@type='disc']">
	    	<li><a href="#tabs-{index}">Disque <xsl:value-of select="index"/></a></li>
	      </xsl:for-each>
	  </ul>
	  <xsl:for-each select="detail">
		  <div id="tabs-{index}">
		  	<div class="item-details3 lightColor">
				<xsl:if test="format/displayname!=''"><b><xsl:value-of select="format/displayname" /></b></xsl:if>
				<xsl:if test="format/displayname!=''"> - </xsl:if><xsl:if test="nrtracks!=''"><span class="item-tracks"><xsl:value-of select="nrtracks"/> Piste<xsl:if test="nrtracks != '1'">s</xsl:if></span></xsl:if>
				<xsl:if test="length!='00:00'"><span class="item-length"> (<xsl:value-of select="length"/>)</span></xsl:if>
   			</div>
   			<xsl:variable name="displayPlaylist">
	   			<xsl:for-each select="details/detail">
	               	<xsl:if test="audiofile!=''">
	                	<xsl:variable name="extension">
						    <xsl:call-template name="get-file-extension">
						        <xsl:with-param name="path" select="audiofile" />
						    </xsl:call-template>
						</xsl:variable>
						<xsl:if test="$extension='mp3'">
							mp3
			            </xsl:if>
	               	</xsl:if>
		    	</xsl:for-each>
   			</xsl:variable>
   			<xsl:if test="$displayPlaylist!=''">
	   			<object type="application/x-shockwave-flash" data="./../script/dewplayer-multi.swf" width="240" height="20" id="dewplayermulti" name="dewplayermulti">
					<param name="movie" value="./../script/dewplayer-multi.swf&amp;xml=./../files/{$value}/cd{index}/playlist.xml&amp;showtime=1&amp;fading=3" />
					<param name="flashvars" value="xml=./../files/{$value}/cd{index}/playlist.xml&amp;showtime=1&amp;fading=3" />
				</object>
            </xsl:if>
		  	<div class="clearfix"></div>
			<hr class="seperator"/>
		  	<h2 class="darkColor">Piste(s)</h2>
	        <table class="table table-hover table-striped table-condensed x-collection">
				<xsl:variable name="cdId"><xsl:value-of select="index" /></xsl:variable>
		        <xsl:for-each select="details/detail">
		              <tr>
		                <td class="rank lightColor"><xsl:value-of select="position"/></td>
		                <td class="track"><xsl:value-of select="title"/></td>
		                <td class="track" align="right">
		                	<xsl:if test="audiofile!=''">
			                	<xsl:variable name="extension">
								    <xsl:call-template name="get-file-extension">
								        <xsl:with-param name="path" select="audiofile" />
								    </xsl:call-template>
								</xsl:variable>
								<!-- example use in branching logic -->
								<xsl:choose>
								    <xsl:when test="$extension = 'mp3'">
				                		<object type="application/x-shockwave-flash" data="./../script/dewplayer.swf" width="200" height="20" id="dewplayerclassic" name="dewplayerclassic">
											<param name="movie" value="./../script/dewplayer.swf?mp3=./../files/{$value}/cd{$cdId}/{index}&amp;showtime=1" />
											<param name="flashvars" value="mp3=./../files/{$value}/cd{$cdId}/{index}.mp3&amp;showtime=1" />
										</object>
								    </xsl:when>
								    <xsl:when test="$extension = 'flv'">
								    	<object type="application/x-shockwave-flash" data="./../script/dewtube.swf" width="512" height="384">
											<param name="movie" value="./../files/{$value}/cd{$cdId}/{index}.flv" />
											<param name="flashvars" value="movie=./../files/{$value}/cd{$cdId}/{index}.flv&amp;width=512&amp;height=384" />
										</object>
								    </xsl:when>
								</xsl:choose>
		                	</xsl:if>
						</td>
		                <td class="length lightColor"><xsl:if test="length!='00:00'"><span class="item-length"><xsl:value-of select="length"/></span></xsl:if></td>
		              </tr>
		    	</xsl:for-each>
	         </table>
		  </div>
      </xsl:for-each>
</div>
</xsl:template>
</xsl:stylesheet>