package aka.xlstexport.main.constants;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;

/**
 * List of property constants.
 *
 * @author Akazukin
 */
public class PropertyConstants {

	/**
	 * Map of music properties.
	 */
	@NonNull
	public static Map<String, @NonNull String> MUSIC_PROPERTIES;
	/**
	 * Map of game properties.
	 */
	@NonNull
	public static Map<String, @NonNull String> GAME_PROPERTIES;
	/**
	 * Map of book properties.
	 */
	@NonNull
	public static Map<String, @NonNull String> BOOK_PROPERTIES;

	/**
	 * XML file.
	 */
	@NonNull
	public final static String XML_FILE = "xml";
	/**
	 * XSL index file.
	 */
	@NonNull
	public final static String XSL_INDEX = "xslIndex";
	/**
	 * XSL details file.
	 */
	@NonNull
	public final static String XSL_DETAILS = "xslDetails";
	/**
	 * XSL cat file.
	 */
	@NonNull
	public final static String XSL_PLATFORMS = "xslPlatforms";
	/**
	 * Export directory.
	 */
	@NonNull
	public final static String HTML_DIR = "html";
	/**
	 * Copy image(s).
	 */
	@NonNull
	public final static String IMAGE = "image";
	/**
	 * Compress CSS.
	 */
	@NonNull
	public final static String COMPRESS_CSS = "compressCSS";
	/**
	 * Compress HTML.
	 */
	@NonNull
	public final static String COMPRESS_HTML = "compressHTML";
	/**
	 * Split by platforms.
	 */
	@NonNull
	public final static String SPLIT_BY_PLATFORM = "splitByPlatform";
	/**
	 * Copy new files or files of different sizes.
	 */
	@NonNull
	public final static String COPY_NEW_DIFFERENT_SIZE = "copyNewDifferentSize";
	/**
	 * Obfuscate CSS.
	 */
	@NonNull
	public final static String OBFUSCATE_JS = "obfuscateJS";
	/**
	 * Sort by title.
	 */
	@NonNull
	public final static String SORT_BY_TITLE = "sortByTitle";
	/**
	 * Copy media file(s).
	 */
	@NonNull
	public final static String MEDIA = "media";
	/**
	 * Generate thumbnail.
	 */
	@NonNull
	public final static String THUMBNAIL = "thumbnail";
	/**
	 * Width of thumbnail.
	 */
	@NonNull
	public final static String THUMBNAIL_SIZE = "thumbnailSize";
	/**
	 * Split index every x item(s).
	 */
	@NonNull
	public final static String SPLIT_EVERY = "splitEvery";
	/**
	 * Brut copy of directory.
	 */
	@NonNull
	public final static String BRUT_COPY = "brutCopy";
	/**
	 * Css File.
	 */
	@NonNull
	public final static String CSS_FILE = "cssFile";

	static {
		MUSIC_PROPERTIES = new HashMap<>();
		MUSIC_PROPERTIES.put(XML_FILE, "musicXML");
		MUSIC_PROPERTIES.put(XSL_INDEX, "musicXslIndex");
		MUSIC_PROPERTIES.put(XSL_DETAILS, "musicXslDetails");
		MUSIC_PROPERTIES.put(HTML_DIR, "musicHTML");
		MUSIC_PROPERTIES.put(IMAGE, "musicImage");
		MUSIC_PROPERTIES.put(MEDIA, "musicMedia");
		MUSIC_PROPERTIES.put(THUMBNAIL, "musicThumbnail");
		MUSIC_PROPERTIES.put(THUMBNAIL_SIZE, "musicThumbnailSize");
		MUSIC_PROPERTIES.put(SPLIT_EVERY, "musicSplitEvery");
		MUSIC_PROPERTIES.put(BRUT_COPY, "musicBrutCopy");
		MUSIC_PROPERTIES.put(CSS_FILE, "musicCssFile");
		MUSIC_PROPERTIES.put(SORT_BY_TITLE, "musicSortByTitle");
		MUSIC_PROPERTIES.put(COMPRESS_CSS, "musicCompressCSS");
		MUSIC_PROPERTIES.put(COMPRESS_HTML, "musicCompressHTML");
		MUSIC_PROPERTIES.put(OBFUSCATE_JS, "musicObfuscateJS");
		MUSIC_PROPERTIES.put(COPY_NEW_DIFFERENT_SIZE, "musicCopyNewDifferentSize");
		GAME_PROPERTIES = new HashMap<>();
		GAME_PROPERTIES.put(XML_FILE, "gameXML");
		GAME_PROPERTIES.put(XSL_INDEX, "gameXslIndex");
		GAME_PROPERTIES.put(XSL_DETAILS, "gameXslDetails");
		GAME_PROPERTIES.put(HTML_DIR, "gameHTML");
		GAME_PROPERTIES.put(IMAGE, "gameImage");
		GAME_PROPERTIES.put(MEDIA, "gameMedia");
		GAME_PROPERTIES.put(THUMBNAIL, "gameThumbnail");
		GAME_PROPERTIES.put(THUMBNAIL_SIZE, "gameThumbnailSize");
		GAME_PROPERTIES.put(SPLIT_EVERY, "gameSplitEvery");
		GAME_PROPERTIES.put(BRUT_COPY, "gameBrutCopy");
		GAME_PROPERTIES.put(CSS_FILE, "gameCssFile");
		GAME_PROPERTIES.put(SORT_BY_TITLE, "gameSortByTitle");
		GAME_PROPERTIES.put(COMPRESS_CSS, "gameCompressCSS");
		GAME_PROPERTIES.put(COMPRESS_HTML, "gameCompressHTML");
		GAME_PROPERTIES.put(OBFUSCATE_JS, "gameObfuscateJS");
		GAME_PROPERTIES.put(COPY_NEW_DIFFERENT_SIZE, "gameCopyNewDifferentSize");
		GAME_PROPERTIES.put(SPLIT_BY_PLATFORM, "gameSplitByPlatform");
		GAME_PROPERTIES.put(XSL_PLATFORMS, "gameXslPlatforms");
		BOOK_PROPERTIES = new HashMap<>();
		BOOK_PROPERTIES.put(XML_FILE, "bookXML");
		BOOK_PROPERTIES.put(XSL_INDEX, "bookXslIndex");
		BOOK_PROPERTIES.put(XSL_DETAILS, "bookXslDetails");
		BOOK_PROPERTIES.put(HTML_DIR, "bookHTML");
		BOOK_PROPERTIES.put(IMAGE, "bookImage");
		BOOK_PROPERTIES.put(MEDIA, "bookMedia");
		BOOK_PROPERTIES.put(THUMBNAIL, "bookThumbnail");
		BOOK_PROPERTIES.put(THUMBNAIL_SIZE, "bookThumbnailSize");
		BOOK_PROPERTIES.put(SPLIT_EVERY, "bookSplitEvery");
		BOOK_PROPERTIES.put(BRUT_COPY, "bookBrutCopy");
		BOOK_PROPERTIES.put(CSS_FILE, "bookCssFile");
		BOOK_PROPERTIES.put(SORT_BY_TITLE, "bookSortByTitle");
		BOOK_PROPERTIES.put(COMPRESS_CSS, "bookCompressCSS");
		BOOK_PROPERTIES.put(COMPRESS_HTML, "bookCompressHTML");
		BOOK_PROPERTIES.put(OBFUSCATE_JS, "bookObfuscateJS");
		BOOK_PROPERTIES.put(COPY_NEW_DIFFERENT_SIZE, "bookCopyNewDifferentSize");
	}

	private PropertyConstants() {
		// Nothing to do
	}
}
